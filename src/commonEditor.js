import _ from 'lodash';
import EditorJS from "@editorjs/editorjs";
const { setItem, getItem, getFileNamesKey } = require("./util/localstorage");
const { DefaultData } = require("./block/data");
const { EDITOR_JS_TOOLS } = require("./constants");

var Editor = function () {
}
Editor.prototype.init = function (option, react_component) {
    this.option = option;
    this.fileData = option.fileData;
    delete option.fileData;
    this.react_component = react_component;
    this.option.data = getItem(this.itemKey()) || this.option.data || DefaultData
    this.editorInstance = new EditorJS(_.assign(this.option, {
        holder: this.option.holder || 'editorjs',
        data: JSON.parse(this.option.data),
        tools: EDITOR_JS_TOOLS,
        // onChange: () => { alert('Now I know that Editor\'s content changed!') }
    }));
}
Editor.prototype.get = function () {
    return this.editorInstance;
}
/**
 * get current content
 */
Editor.prototype.save = async function () {
    let content = await this.editorInstance.save()
    delete content.time;
    return JSON.stringify(content);
}
Editor.prototype.itemKey = function () {
    return this.fileData && this.fileData.fileKey &&
        (getFileNamesKey() + "." + this.fileData.fileKey);
}
Editor.prototype.saveCurrentFile = async function () {
    let outputData = await this.save();
    let key = this.itemKey();
    let oldData = (getItem(key)) || this.option.data;
    if (key && outputData !== oldData) {
        setItem(key, outputData);//in browser cache 
        this.fileData.mtimeMs = (new Date()).getTime();
        this.option.data = outputData;
        return true;
    }
    return false;
}
Editor.prototype.value = function (content) {
    this.clearCache();
    this.option.data = content || DefaultData
    document.getElementById("drawer").innerHTML = ""
    this.editorInstance.render(JSON.parse(this.option.data))
}
Editor.prototype.text = function () {
    return this.option.data || DefaultData;
}
Editor.prototype.clearCache = function () {
    if (this.autosaveTimeoutId) {
        clearTimeout(this.autosaveTimeoutId);
        this.autosaveTimeoutId = undefined;
    }
}
Editor.prototype.autosave = async function () {
    this.clearCache();
    this.autosaveTimeoutId = setTimeout(() => {
        this.saveCurrentFile().then((save) => {
            save && this.react_component.saveFileNamesJson();
            this.autosave();
        });
    }, this.option.autosaveDelay || 10000);
}
Editor.prototype.setFileData = function (fileData) {
    this.fileData = fileData;
}


const editor = new Editor();
export default editor;