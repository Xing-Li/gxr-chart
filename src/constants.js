import Chart from "./block/chart";
import CodeTool from "./block/code/codeTool";
import SimpleImage from "./block/image/simpleImage";
import CheckList from "@editorjs/checklist";
import Delimiter from "@editorjs/delimiter";
import Embed from "@editorjs/embed";
import Header from "@editorjs/header";
import InlineCode from "@editorjs/inline-code";
import LinkTool from "@editorjs/link";
import List from "@editorjs/list";
import Marker from "@editorjs/marker";
import Quote from "@editorjs/quote";
import Raw from "@editorjs/raw";
import Table from "@editorjs/table";
import Warning from "@editorjs/warning";

export const EDITOR_JS_TOOLS = {
    embed: Embed,
    table: Table,
    list: List,
    warning: Warning,
    codeTool: {
        class: CodeTool,
        contentEditable: true
    },
    linkTool: LinkTool,
    chart: {
        class: Chart,
        contentEditable: false
    },
    header: Header,
    raw: Raw,
    quote: Quote,
    marker: Marker,
    checklist: CheckList,
    delimiter: Delimiter,
    inlineCode: InlineCode,
    simpleImage: SimpleImage
};
