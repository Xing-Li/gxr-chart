import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'counter',
  initialState: {
    value: {},
  },
  reducers: {
    setCount: (state, action) => {
      state.value = action.payload;
    },
  },
});

export const { setCount } = slice.actions;

export const getCount = state => state.counter.value;

export default slice.reducer;
