import { BackTop } from 'antd';
import React, { Component } from "react";
import Wrapper from "./Wrapper";

export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    return <React.Fragment>
      <Wrapper></Wrapper>
      <BackTop />
    </React.Fragment>;
  }
}