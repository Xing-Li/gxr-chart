// Here are graph operation draft codes.
// Need to be merged into graph core later

import _ from 'lodash';
import { Easing, Tween, autoPlay } from 'es6-tween';

const API = window.opener._app.controller.API;
const _GXR = window.opener._GXR;
const _app = window.opener._app;
const _AppActions = window.opener._AppActions;
const TaskList = window.opener._GXR.TaskList;
const _drawing = window.opener._drawing;

const _graph = _drawing.graph;
const _edges = _graph.edges;

window._graph = _graph;
// Unitlities

const VizControl = {
	selectNodesById: (nodeIds) => {
		API.selectNodes(nodeIds);
	},
	deSelectNodesById: (nodeIds) => {
		API.deSelectNodes(nodeIds);
	},
	hideNodesById: (nodeIds) => {
		_GXR.NodesPinManager.pinNodeIds(nodeIds, true);
		_GXR.NodesSelectManager.selectWithNodeIds(
			nodeIds,
			_GXR.NodesSelectManager.selectTypes.removeSub,
		);
		_.remove(_graph.layoutNodes, (n) => {
			return nodeIds.findIndex((m) => m === n.id) > -1;
		});
		_.remove(_graph.layoutEdges, (n) => {
			return (
				nodeIds.findIndex((m) => m === n.targetId || m === n.sourceId) > -1
			);
		});
		// _app.controller.drawing.forceStartLayout();
	},
	unHideNodesById: (nodeIds) => {
		const allVisibleNodeIds = _.uniq(
			_.flatten([_graph.layoutNodes.map((n) => n.id), nodeIds]),
		);

		const nodesToUnhide = _graph.nodes.filter((n) => nodeIds.includes(n.id));
		const edgesToUnhide = _graph.edges.filter(
			(e) =>
				allVisibleNodeIds.includes(e.sourceId) &&
				allVisibleNodeIds.includes(e.targetId),
		);

		_graph.layoutNodes = _.union(_graph.layoutNodes, nodesToUnhide);
		_graph.layoutEdges = _.union(_graph.layoutEdges, edgesToUnhide);
	},
	hideCategory: (category) => {
		const nodeIds = getNodesByLabel(category).map((n) => n.id);
		VizControl.hideNodesById(nodeIds);
	},
	unHideCategory: (category) => {
		const nodeIds = getNodesByLabel(category).map((n) => n.id);
		VizControl.unHideNodesById(nodeIds);
	},
};

window._VizControl = VizControl;

function addEdge({ src, tgt, extra }) {
	_graph.addEdge(src, tgt, extra);
}

function deleteEdgeByRelationship(relationship) {
	const ids = _graph.edges
		.filter((e) => e.name === relationship)
		.map((e) => e.id);
	_graph.removeEdgesByIds(ids);
}

function startLayout() {
	_drawing.forceStartLayout();
}

function getNodesByLabel(label) {
	return _.filter(_graph.nodes, (n) => n.data.detail.type === label);
}

function deleteNodesByLabel(label) {
	const ids = getNodesByLabel(label).map((n) => n.id);
	_graph.removeNodesByIds(ids);
}

function deleteNodesByIds(ids) {
	_graph.removeNodesByIds(ids);
}

const genUniqNodeId = function () {
	return (
		Date.now().toString(36) + Math.random().toString(36).slice(2, 7)
	).toUpperCase();
};

const getNodesByLabelProps = function (label, matches) {
	let subset = getNodesByLabel(label);
	subset = _.reduce(
		Object.keys(matches),
		(res, key) => {
			return res.filter((n) => n.data.detail.data[key] === matches[key]);
		},
		subset,
	);

	// For(key in Object.keys(matches)){
	//     subset = subset.filter(n=> n.data.detail.data[key] == matches[key])
	// }
	return subset;
};

function zfill(number, length_) {
	return (new Array(length_).join('0') + number).slice(-length_);
}

const getMonth = function (st) {
	const convert = {
		January: 1,
		February: 2,
		March: 3,
		april: 4,
		May: 5,
		June: 6,
		July: 7,
		augustus: 8,
		september: 9,
		October: 10,
		november: 11,
		december: 12,
	};
	if (st.includes(',')) {
		return zfill(convert[st.split(',')[0].split(' ')[0]], 2);
	}

	return zfill(convert[st.split(' ')[1]], 2);
};

const getYear = function (st) {
	const array = st.split(' ');
	return array[array.length - 1];
};

const getDay = function (st) {
	if (st.includes(',')) {
		return zfill(st.split(',')[0].split(' ')[1], 2);
	}

	return zfill(st.split(' ')[0], 2);
};

// ========================================================================
// Graph operations
// Change label of existing nodes
function changeLabel(fromLabel, toLabel) {
	getNodesByLabel(fromLabel).forEach((n) => {
		n.data.detail.type = toLabel;
		n.color.setStyle(_GXR.TypeColor.getColorByType(toLabel));
	});
}

// Need a interface to change property or create new property
function randomizeNodePosition(node) {
	node.position.x = Math.random() - 0.5;
	node.position.y = Math.random() - 0.5;
	node.position.z = Math.random() - 0.5;
}

function moveNodesTo(nodes, targetPositions, duration, onComplete) {
	autoPlay(true); // Simplify the your code

	TaskList.pinNode.func(nodes.map((n) => n.id));
	const initPositions = nodes.map((n) => n.position.clone());

	const parameter = { t: 0 };
	const cnt = nodes.length;
	const tween = new Tween(parameter)
		.to({ t: 1 }, duration)
		.on('update', ({ t }) => {
			console.log(`The values is x: ${t}`);
			_.range(cnt).forEach((i) => {
				nodes[i].position.copy(
					initPositions[i]
						.clone()
						.multiplyScalar(1 - t)
						.add(targetPositions[i].clone().multiplyScalar(t)),
				);
			});
		})
		.on('complete', onComplete)
		.start();
}

const getAllConnectedNodeIds = (nodes) => {
	const nodeSet = {};
	_graph.layoutNodes.forEach((n) => {
		nodeSet[n.id] = n;
	});

	const collectingConnectedNodeIds = (nodeIds, collectedIds) => {
		const nodes = nodeIds.map((id) => nodeSet[id]);
		const allNeighborIds = _.uniq(
			_.flatten(nodes.filter((n) => n !== undefined).map((n) => n.neighbor)),
		);
		const newNodeIds = _.difference(allNeighborIds, collectedIds);
		const allCollectedIds = _.flatten([newNodeIds, collectedIds]);
		return newNodeIds.length > 0
			? collectingConnectedNodeIds(newNodeIds, allCollectedIds)
			: allCollectedIds;
	};

	const nodeIds = nodes.map((n) => n.id);
	return collectingConnectedNodeIds(nodeIds, nodeIds);
};

function genNodesFromJsonArray(dataList, label) {
	const nodes = dataList.map((data) => {
		// What if id is an existing field?
		data.id = genUniqNodeId();

		const node = new _GXR.Node(data.id);

		node.data.detail = {
			type: label,
			data,
		};

		node.color.setStyle(_GXR.TypeColor.getColorByType(label));
		randomizeNodePosition(node);
		return node;
	});

	_graph.addNodes(nodes);

	_drawing.forceStartLayout();

	return nodes;
}

// Loop over nodes in a label, create new nodes from it
// usually call this from genDependentNodes()
const addOrMergeNodeFromLabel = function (label, uniqKeys, data) {
	const existNodes = getNodesByLabelProps(label, _.pick(data, uniqKeys));
	if (existNodes.length > 0) {
		// If more than 1 match found, we merge with the first one
		existNodes[0].data.detail.data = {
			...existNodes[0].data.detail.data,
			...data,
		};
		return existNodes[0];
	}

	// If data doesn't id, or it does, but already in nodes
	if (
		data.id === undefined ||
		_.map(_graph.nodes, 'id').filter((x) => x === data.id).length > 0
	) {
		data.id = genUniqNodeId();
	}

	const n = new _GXR.Node(data.id);

	n.data.detail = {};

	n.data.detail.type = label;
	n.data.detail.data = { ...data };
	// Assign random position, need to be overwrited if position already defined
	n.position.x = Math.random() - 0.5;
	n.position.y = Math.random() - 0.5;
	n.position.z = Math.random() - 0.5;
	n.color.setStyle(_GXR.TypeColor.getColorByType(label));
	randomizeNodePosition(n);
	_graph.addNode(n);
	return n;
};

// From nodes with sourceLabel, create new nodes with data in those nodes, and link to source
// @Weidong next: if uniqKey is empty, each node map to a new node
const genDependentNodes = function (
	sourceLabel,
	label,
	relationship,
	keys,
	uniqKeys,
) {
	const edges = [];
	getNodesByLabel(sourceLabel).forEach((n0) => {
		const data = _.pick(n0.data.detail.data, keys);
		const n = addOrMergeNodeFromLabel(label, uniqKeys, data);
		const edge = new _GXR.Edge(n0, n, {
			name: relationship,
		});
		edges.push(edge);
	});
	_graph.addEdges(edges);
};

function buildEdgeMaps() {
	_graph.nodes.forEach((n) => (n.connectedEdges = []));
	_edges.forEach((e) => {
		e.source.connectedEdges.push(e);
		e.target.connectedEdges.push(e);
	});
}

// Create short cut between two nodes separated by one middle node
// Find  (centerLabel)-[relation1]-(n1)
//  (centerLabel)-[relation2]-(n2)
// create (n1)-[newRelation]-(n2)
function shortCut({ centerLabel, relation1, relation2, newRelation }) {
	buildEdgeMaps();
	const sourceNodes = _.filter(
		_graph.layoutNodes,
		(n) => n.data.detail.type === centerLabel,
	);
	const edges = [];
	sourceNodes.forEach((n0) => {
		const es1 = n0.connectedEdges.filter((e) => e.name === relation1);
		// Let es1 = _graph.getEdgesToNode(n0).filter(e=>e.name == relation1)
		const ns1 = _.flatten(es1.map((e) => [e.source, e.target])).filter(
			(n) => n.id !== n0.id,
		);

		const es2 = n0.connectedEdges.filter((e) => e.name === relation2);
		// Let es2 = _graph.getEdgesToNode(n0).filter(e=>e.name == relation2)
		const ns2 = _.flatten(es2.map((e) => [e.source, e.target])).filter(
			(n) => n.id !== n0.id,
		);
		ns1.forEach((n1) => {
			ns2.forEach((n2) => {
				if (
					n1 !== n2 && // No self loop edge
					!_graph.getEdge(n1, n2) &&
					!_graph.getEdge(n2, n1) &&
					// Edge is non-directionsl
					edges.filter(
						(e) =>
							(e.source == n1 && e.target === n2) ||
							(e.source === n2 && e.target === n1),
					).length === 0
				) {
					const edge = new _GXR.Edge(n1, n2, {
						name: newRelation,
					});
					edges.push(edge);
					// _graph.addEdges([edge])
				}
			});
		});
	});
	_graph.addEdges(edges);
}

// Clean up properties from nodes
function removeProps(label, keys) {
	getNodesByLabel(label).forEach((n) => {
		keys.forEach((k) => {
			delete n.data.detail.data[k];
		});
	});
}

// Once a node assigned to map, we need to explicitly remove it from the map
function clearLatLng(label) {
	getNodesByLabel(label).forEach((n) => {
		delete n.data.hasLatLng;
		delete n.data.lat;
		delete n.data.lng;
	});
}

// ========================================================================
function mergeNodes(nodes) {
	const toDrop = [];

	const n0 = nodes.pop();
	nodes.forEach((n) => {
		const asSource = _edges.filter((e) => e.source === n);
		asSource.forEach((e) => {
			const e1 = _graph.addEdge(n0, e.target, {
				name: e.name,
			});
			_graph.removeEdge(e);
		});
		const asTarget = _edges.filter((e) => e.target === n);
		asTarget.forEach((e) => {
			const e1 = _graph.addEdge(e.source, n0, {
				name: e.name,
			});
			_graph.removeEdge(e);
		});
		toDrop.push(n);
	});

	_graph.removeNodesByIds(toDrop.map((n) => n.id));
}

function mergeNodesByLabelProp(label, propKeys) {
	// Label = 'kmzImport_Point'
	// propKeys = ['Email', 'Distance']
	const nodes = getNodesByLabel(label);
	const grouped = _.groupBy(nodes, (n) =>
		propKeys.map((k) => n.data.detail.data[k]).join('-'),
	);

	const toDrop = [];
	_.forEach(grouped, (ns, k) => {
		// We keep the last node, delete all others, but move the link to the node0
		const n0 = ns.pop();
		ns.forEach((n) => {
			const asSource = _edges.filter((e) => e.source === n);
			asSource.forEach((e) => {
				const e1 = _graph.addEdge(n0, e.target, {
					name: e.name,
				});
				_graph.removeEdge(e);
			});
			const asTarget = _edges.filter((e) => e.target === n);
			asTarget.forEach((e) => {
				const e1 = _graph.addEdge(e.source, n0, {
					name: e.name,
				});
				_graph.removeEdge(e);
			});
			toDrop.push(n);
		});
	});
	_graph.removeNodesByIds(toDrop.map((n) => n.id));
}

//  Super node
function createNode(label, data, nodeId) {
	const n = new _GXR.Node(nodeId || genUniqNodeId());
	n.data.detail = {};

	n.data.detail.type = label;
	n.data.detail.data = data|| {};
	// Assign random position, need to be overwrited if position already defined
	n.position.x = Math.random() - 0.5;
	n.position.y = Math.random() - 0.5;
	n.position.z = Math.random() - 0.5;
	n.color.setStyle(_GXR.TypeColor.getColorByType(label));

	_graph.addNode(n);
	return n;
}

function groupNodesToSuperNode(nodes, sNodeLabel) {
	const sNode = createNode(sNodeLabel);

	const childLabels = [];
	nodes.forEach((n) => {
		const asSource = _edges.filter((e) => e.source === n);
		asSource.forEach((e) => {
			const e1 = _graph.addEdge(sNode, e.target, {
				name: e.name,
			});
		});
		const asTarget = _edges.filter((e) => e.target === n);
		asTarget.forEach((e) => {
			const e1 = _graph.addEdge(e.source, sNode, {
				name: e.name,
			});
		});

		const e1 = _graph.addEdge(sNode, n, {
			name: 'has_gxr_child',
		});

		const toLabel = 'gxrChild_' + n.data.detail.type;
		childLabels.push(toLabel);
		n.data.detail.type = toLabel;
		n.color.setStyle(_GXR.TypeColor.getColorByType(toLabel));
	});

	_.uniq(childLabels).forEach((l) => {
		_GXR.TaskList.showNodesByLabel.func(l + ',0');
	});

	return sNode;
}

function reverseSuperNodeToChildren(sNode) {
	const children = _edges
		.filter((e) => e.source === sNode)
		.map((e) => e.target);
	const labels = [];
	children.forEach((n) => {
		const label = n.data.detail.type.split('gxrChild_')[1];
		// _graph.layoutNodes.push(n)
		n.data.detail.type = label;
		n.color.setStyle(_GXR.TypeColor.getColorByType(label));
		labels.push(label);
	});
	_.uniq(labels).forEach((l) => {
		_GXR.TaskList.showNodesByLabel.func(l + ',0');
		_GXR.TaskList.showNodesByLabel.func(l + ',1');
	});
	_graph.removeNodeById(sNode.id);
	_drawing.forceStartLayout();
}

// ========================================================================
// special function for kmz, need to provide a key for sorting
function linkSequentialNodesForLabel(label) {
	const sortedNodes = _.sortBy(
		getNodesByLabel(label),
		(n) => n.data.detail.data.callDate,
	);
	const edges = [];
	for (let i = 0; i < sortedNodes.length - 1; i += 1) {
		const edge = new _GXR.Edge(sortedNodes[i], sortedNodes[i + 1], {
			name: 'sequential',
		});
		edges.push(edge);
	}

	_graph.addEdges(edges);
}

function linkSepectedNodes(nodes, relationship) { }

// Function aggregate(toLabel, relationship, prop, toProp, aggregFunc){
// toProp is single, prop is single, aggregFunc(fromVal, fromNodeData, toNodeData)
function aggregate({
	toLabel,
	relationship,
	prop,
	toProp,
	aggregFunc,
	reduceFunc,
}) {
	const toNodes = getNodesByLabel(toLabel);
	toNodes.forEach((toNode) => {
		const edges = _drawing.graph
			.getEdgesToNode(toNode)
			.filter((e) => e.name === relationship);

		let results = edges.map((e) => {
			const testNodes = [e.source, e.target];
			_.remove(testNodes, toNode);
			const fromNode = testNodes[0];

			if (aggregFunc === undefined) {
				// ToNode.data.detail.data[toProp] = fromNode.data.detail.data[prop]
				return fromNode.data.detail.data[prop];
			}

			// AggregFunc(fromVal, fromData, toData)
			// toNode.data.detail.data[toProp] = aggregFunc(fromNode.data.detail.data[prop], fromNode.data.detail.data, toNode.data.detail.data)
			return aggregFunc(
				fromNode.data.detail.data[prop],
				fromNode.data.detail.data,
				toNode.data.detail.data,
			);
		});

		results = results.filter((res) => res !== null);
		if (results.length > 0) {
			if (reduceFunc === undefined)
				toNode.data.detail.data[toProp] = results[0];
			else toNode.data.detail.data[toProp] = reduceFunc(results);
		}
	});
}

function map({ label, props, toProp, mapFunc }) {
	const nodes = getNodesByLabel(label);
	nodes.forEach((n) => {
		n.data.detail.data[toProp] = mapFunc(n, props);
	});
}

function link({
	srcLabel,
	tgtLabel,
	newRelation,
	srcProp,
	tgtProp,
	matchFunc,
}) {
	matchFunc = matchFunc === undefined ? (a, b) => a === b : matchFunc;

	const srcNodes = getNodesByLabel(srcLabel);
	const tgtNodes = getNodesByLabel(tgtLabel);

	const edges = [];
	srcNodes.forEach((n1) => {
		tgtNodes.forEach((n2) => {
			const d1 = n1.data.detail.data[srcProp];
			const d2 = n2.data.detail.data[tgtProp];
			if (
				d1 !== null &&
				d1 !== undefined &&
				d2 !== undefined &&
				d2 !== null &&
				matchFunc(d1, d2)
			) {
				if (
					n1 !== n2 && // No self loop edge
					!_graph.getEdge(n1, n2) &&
					!_graph.getEdge(n2, n1) &&
					// Edge is non-directionsl
					edges.filter(
						(e) =>
							(e.source === n1 && e.target === n2) ||
							(e.source === n2 && e.target === n1),
					).length === 0
				) {
					const edge = new _GXR.Edge(n1, n2, {
						name: newRelation,
					});
					edges.push(edge);
					// _graph.addEdges([edge])
				}
			}
		});
	});
	_graph.addEdges(edges);
}

// =================== Utilities ==================

function hideToolPanel() {
	_app.controller.store.dispatch(_AppActions.showTopbar(''));
}

export {
	_graph,
	_drawing,
	_GXR,
	startLayout,
	moveNodesTo,
	getNodesByLabel,
	getNodesByLabelProps,
	getAllConnectedNodeIds,
	deleteNodesByIds,
	addEdge,
	deleteEdgeByRelationship,
	addOrMergeNodeFromLabel,
	genNodesFromJsonArray,
	deleteNodesByLabel,
	// GraphOps
	aggregate,
	map,
	shortCut,
	link,
	// Utilities
	hideToolPanel,
	VizControl,
	createNode,
};
