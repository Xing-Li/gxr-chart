import {
  Form, Input, InputNumber,
  Select, Space, Cascader, Switch, Tabs, Slider,
  Table, Menu, Dropdown, Popconfirm,
} from 'antd';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import CodeMirror from 'codemirror'
import { Controlled } from 'react-codemirror2';
import 'codemirror';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/addon/hint/show-hint';
import 'codemirror/addon/hint/javascript-hint';
import 'codemirror/addon/fold/foldcode'
import 'codemirror/addon/fold/foldgutter'
import 'codemirror/addon/fold/comment-fold'
import 'codemirror/addon/comment/comment'
import showHint from './util/CodeMirror.showHint.js'
import { CategoryPropertyChooser } from './components';
const { getNodesByLabel } = require('./common/graphOps.js');
const {
  ChartTypes, DefaultChartConfig, DefaultNameLocationConfig,
  getType, SelectProps, Themes, TypeObj, Types,
  SortOptions, sortFunc, CodeOptions, transfor, showToast, CategoryFromType,
  ShareData
} = require("./util/utils");
const { showObject, showWrapperObject } = require('./util/helpers.js');

const { Option } = Select;
const { Column, ColumnGroup } = Table;
const { TextArea } = Input;
const { TabPane } = Tabs;
const XFuncTypes = {
  xAxisFormatter: { type: "xAxisFormatter", optionLabel: "formatter" },
  xAxisMin: { type: "xAxisMin", optionLabel: "min" },
  xAxisMax: { type: "xAxisMax", optionLabel: "max" }
}
const YFuncTypes = {
  yAxisFormatter: { type: "yAxisFormatter", optionLabel: "formatter" },
  yAxisMin: { type: "yAxisMin", optionLabel: "min" },
  yAxisMax: { type: "yAxisMax", optionLabel: "max" }
}
export default class ChartSetting extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    run: PropTypes.func,
    publish: PropTypes.func,
    chartConfig: PropTypes.shape({
      chartKey: PropTypes.string,
      category: PropTypes.string,
      propertyMap: PropTypes.object,
      chartTypeSelected: PropTypes.oneOf(_.keys(ChartTypes)),
      source: PropTypes.array,
      width: PropTypes.number,
      height: PropTypes.number,
      title: PropTypes.string,
      minMaxMap: PropTypes.object,
      dataZoom: PropTypes.bool
    }),
  };

  static defaultProps = {};
  constructor(props) {
    super(props);
    const chartConfigTmp = transfor(_.cloneDeep(this.props.chartConfig))
    this.state = chartConfigTmp;
    this.oldChartConfig = _.cloneDeep(chartConfigTmp);
  }

  componentDidUpdate(prevProps, prevState) {
    const { chartConfig } = this.props
    if (prevProps.chartConfig !== chartConfig || !_.isEqual(prevProps.chartConfig, chartConfig)) {
      const chartConfigTmp = transfor(_.cloneDeep(this.props.chartConfig))
      this.setState(chartConfigTmp);
      this.oldChartConfig = _.cloneDeep(chartConfigTmp);
    }
  }

  componentDidMount() {
  }

  handleCatetoryPropertySelected = (categoryFrom, category, properties, minMaxMapT, chartTypesT) => {
    const minMaxMap = minMaxMapT || this.state.minMaxMap || {};
    const { selected, sortOption, propertyMap } = this.state;
    const { run } = this.props;
    let propertyMapTmp = _.reduce(properties, (prevObj, currProperty, index) => {
      prevObj[currProperty] = {};
      return prevObj;
    }, {});
    let source;
    switch (categoryFrom) {
      case CategoryFromType.main:
        source = _.reduce(getNodesByLabel(category), (prev, curr) => {
          prev.push(_.reduce(properties, (prevArr, currP, index) => {
            if (selected && curr.selected || !selected) {
              let value = curr.data.detail.data[currP];
              let min = (minMaxMap[currP] && minMaxMap[currP].min) || 0;
              let max = (minMaxMap[currP] && minMaxMap[currP].max) || 0;
              if (!propertyMapTmp[currP].type) {
                propertyMapTmp[currP] = {
                  type: typeof value, max: 0, min: 0
                }
              }
              if (propertyMapTmp[currP].type === "number") {
                if (min === max || (min < max && min <= value && value <= max)) {
                  prevArr.push(value)
                }
                propertyMapTmp[currP].min = propertyMapTmp[currP].min > value ?
                  value : propertyMapTmp[currP].min;
                propertyMapTmp[currP].max = propertyMapTmp[currP].max < value ?
                  value : propertyMapTmp[currP].max;
              } else {
                prevArr.push(value)
              }
            }
            return prevArr
          }, []))
          return prev
        }, []);
        break;
      case CategoryFromType.shareData:
        source = _.reduce(ShareData[category], (prev, curr) => {
          prev.push(_.reduce(properties, (prevArr, currP, index) => {
            let value = curr[currP];
            let min = (minMaxMap[currP] && minMaxMap[currP].min) || 0;
            let max = (minMaxMap[currP] && minMaxMap[currP].max) || 0;
            if (!propertyMapTmp[currP].type) {
              propertyMapTmp[currP] = {
                type: typeof value, max: 0, min: 0
              }
            }
            if (propertyMapTmp[currP].type === "number") {
              if (min === max || (min < max && min <= value && value <= max)) {
                prevArr.push(value)
              }
              propertyMapTmp[currP].min = propertyMapTmp[currP].min > value ?
                value : propertyMapTmp[currP].min;
              propertyMapTmp[currP].max = propertyMapTmp[currP].max < value ?
                value : propertyMapTmp[currP].max;
            } else {
              prevArr.push(value)
            }
            return prevArr
          }, []))
          return prev
        }, []);
        break;
      default:
        source = [];
        console.error("not implement")
        break;
    }
    sortFunc(sortOption, source)
    let prevProperties = _.keys(propertyMap);
    let state = {
      category: category, categoryFrom: categoryFrom, propertyMap: propertyMapTmp,
      source: source, minMaxMap: minMaxMap
    };
    if (chartTypesT !== undefined) {
      _.assign(state, { chartTypes: chartTypesT })
    }
    const xAxisType = getType(propertyMapTmp, 0);
    const yAxisType = getType(propertyMapTmp, 1);
    if (prevProperties[0] !== properties[0]) {
      _.assign(state, {
        xAxisType: xAxisType,
        xAxisFormatter: TypeObj[xAxisType].formatter,
        xAxisMin: TypeObj[xAxisType].min,
        xAxisMax: TypeObj[xAxisType].max,
        xAxisFormatterTmp: TypeObj[xAxisType].formatter,
        xAxisMinTmp: TypeObj[xAxisType].min,
        xAxisMaxTmp: TypeObj[xAxisType].max
      })
    }
    if (prevProperties[1] !== properties[1]) {
      _.assign(state, {
        yAxisType: yAxisType,
        yAxisFormatter: TypeObj[yAxisType].formatter,
        yAxisMin: TypeObj[yAxisType].min,
        yAxisMax: TypeObj[yAxisType].max,
        yAxisFormatterTmp: TypeObj[yAxisType].formatter,
        yAxisMinTmp: TypeObj[yAxisType].min,
        yAxisMaxTmp: TypeObj[yAxisType].max
      })
    }
    this.setState(state, () => {
      run(this.state)
    });
  };

  formatter(value) {
    return `${value}px`;
  }

  onChangeSort = (value) => {
    this.setState({ sortOption: value }, () => {
      const { sortOption, source } = this.state;
      const { run } = this.props;
      sortFunc(sortOption, source)
      run(this.state)
    })
  }

  testFunc = (key) => {
    const { run } = this.props;
    const state = {};
    const value = this.state[key + 'Tmp'];
    if (!value) return;
    try {//test if common func is ok
      if (!~['dataMin', 'dataMax'].indexOf(value.trim()) && isNaN(value.trim())) {
        let func = new Function(['value', 'index'], value);
        func(0, 0);
        func('', 1);
        func("aaaa", 2)
      }
      state[key] = value;
      state[key + 'Tmp'] = value;
    } catch (e) {
      state[key] = undefined;
      state[key + 'Tmp'] = undefined;
      console.error(e)
    }
    this.setState(state, () => {
      run(this.state)
    });
  }

  render() {
    transfor(this.state);
    const { category, propertyMap, selected, chartTypeSelected,
      chartTypes, minMaxMap, chartKey,
      title, titleGrid, width, height, dataZoom,
      nameLocation, grid, theme, sortOption,
      xAxisType, yAxisType, xAxisName, yAxisName,
      xAxisNameGap, yAxisNameGap, xAxisLabelRotate, yAxisLabelRotate,
      xAxisCode, yAxisCode, xAxisCodeType, yAxisCodeType,
      scale, fontScale, source, categoryFrom
    } = this.state;
    const { run, className } = this.props;
    const properties = _.keys(propertyMap);
    const { axis } = ChartTypes[chartTypeSelected];
    const mode = 'top';
    let datas = _.reduce(source, (prev, curr) => {
      let obj = {};
      _.each(properties, (key, index) => {
        obj[key] = curr[index];
      })
      prev.push(obj);
      return prev;
    }, []);
    return (
      <div className={`header ${className || ''}`}>
        <Tabs defaultActiveKey="1" tabPosition={mode} size={"small"} className={"tabs"} >
          <TabPane tab={`${"main"}`} key={"Tab-main"}>
            <Space className="d-flex flex-wrap align-content-start">
              <Form.Item label="type" className="config-item">
                <Select {...SelectProps}
                  className="config-input" placeholder="Select chart type"
                  value={chartTypeSelected} onChange={(value, option) => {
                    this.setState({ chartTypeSelected: value }, () => {
                      run(this.state)
                    })
                  }}>
                  {
                    _.map(_.keys(ChartTypes), (chartType, index) => {
                      return <Option title={chartType} key={chartType} value={chartType} >{chartType}</Option>
                    })
                  }
                </Select>
              </Form.Item>
              <Form.Item label="title" className="config-item">
                <Input className="config-input" value={title} onChange={(e) => {
                  this.setState({ title: e.target.value }, () => {
                    run(this.state);
                  })
                }} />
              </Form.Item>
              <Form.Item label="width" className="config-item">
                <InputNumber
                  className="config-input"
                  min={5}
                  max={200}
                  value={width}
                  formatter={value => `${value}%`}
                  parser={value => value.replace('%', '')}
                  onChange={(value) => {
                    this.setState({ width: value }, () => {
                      run(this.state);
                    })
                  }}
                />
              </Form.Item>
              <Form.Item label="height" className="config-item">
                <InputNumber
                  className="config-input"
                  min={5}
                  max={200}
                  value={height}
                  formatter={value => `${value}%`}
                  parser={value => value.replace('%', '')}
                  onChange={(value) => {
                    this.setState({ height: value }, () => {
                      run(this.state);
                    })
                  }}
                />
              </Form.Item>
            </Space>
            <CategoryPropertyChooser
              selectedCategory={category}
              selectedCategoryFrom={categoryFrom}
              selectedProperties={properties}
              chartTypeSelected={chartTypeSelected}
              chartTypes={chartTypes || DefaultChartConfig.chartTypes}
              minMaxMap={minMaxMap}
              propertyMap={propertyMap}
              setSelected={(selected) => this.setState({ selected: selected })}
              handleCatetoryPropertySelected={this.handleCatetoryPropertySelected}>
            </CategoryPropertyChooser>
          </TabPane>
          {!ChartTypes[chartTypeSelected].xyAxisExclude &&
            <TabPane tab={`${"xAxis"}`} key={"Tab-xAxis"}>
              <Space className="d-flex flex-wrap align-content-start">
                <Space className="d-flex flex-wrap">
                  <Form.Item label="name" className="config-item">
                    <Input className="config-input" title={xAxisName}
                      value={xAxisName} onChange={(e) => {
                        this.setState({ xAxisName: e.target.value }, () => {
                          run(this.state);
                        })
                      }} />
                  </Form.Item>
                  <Form.Item label="type" className="config-item">
                    <Select {...SelectProps} allowClear={true}
                      className="config-input" placeholder="Select type"
                      value={xAxisType} onChange={(value) => {
                        if (value === undefined) {
                          return;
                        }
                        this.setState({
                          xAxisType: value,
                          xAxisFormatter: TypeObj[value].formatter,
                          xAxisMin: TypeObj[value].min,
                          xAxisMax: TypeObj[value].max,
                          xAxisFormatterTmp: TypeObj[value].formatter,
                          xAxisMinTmp: TypeObj[value].min,
                          xAxisMaxTmp: TypeObj[value].max,
                        }, () => {
                          run(this.state)
                        })
                      }} onClear={() => {
                        this.setState({
                          xAxisType: undefined,
                          xAxisFormatter: undefined,
                          xAxisMin: undefined,
                          xAxisMax: undefined,
                          xAxisFormatterTmp: undefined,
                          xAxisMinTmp: undefined,
                          xAxisMaxTmp: undefined,
                        }, () => {
                          run(this.state)
                        })
                      }}>
                      {
                        _.map(Types, (type, index) => {
                          return <Option title={type} key={type} value={type} >
                            {TypeObj[type].optionLabel}
                          </Option>
                        })
                      }
                    </Select>
                  </Form.Item>
                  <Form.Item label={<span className="code-expand-icon code-box-code-action" onClick={() => {
                    this.setState({ xAxisCode: !xAxisCode }, () => {
                      const { xAxisCode, xAxisCodeType } = this.state;
                      if (!xAxisCode) {
                        this.testFunc(xAxisCodeType || _.keys(XFuncTypes)[0]);
                      }
                    })
                  }} >
                    {xAxisCode ?
                      <img title="shrink code" alt="shrink code" src="/svg/open.svg" className="code-expand-icon-show" />
                      :
                      <img title="expand code" alt="expand code" src="/svg/close.svg" className="code-expand-icon-hide" />
                    }
                  </span>} className="config-item">
                    {xAxisCode && <Select {...SelectProps}
                      className="config-input" placeholder="Select type"
                      value={xAxisCodeType} onChange={(value, option) => {
                        this.setState({ xAxisCodeType: value })
                      }}>
                      {
                        _.map(_.values(XFuncTypes), (funcType, index) => {
                          return <Option title={funcType.type} key={funcType.type} value={funcType.type} >
                            {funcType.optionLabel}
                          </Option>
                        })
                      }
                    </Select>}
                  </Form.Item>
                  <Form.Item label="gap" className="config-item">
                    <InputNumber
                      className="config-input"
                      min={-200}
                      max={200}
                      value={xAxisNameGap || DefaultChartConfig.xAxisNameGap}
                      formatter={value => `${value}px`}
                      parser={value => value.replace('px', '')}
                      onChange={(value) => {
                        this.setState({ xAxisNameGap: value }, () => {
                          run(this.state);
                        })
                      }}
                    />
                  </Form.Item>
                  <Form.Item label="label rotate" className="config-item">
                    <InputNumber
                      className="config-input"
                      value={xAxisLabelRotate}
                      formatter={value => `${value}°`}
                      parser={value => value.replace('°', '')}
                      onChange={(value) => {
                        this.setState({ xAxisLabelRotate: value }, () => {
                          run(this.state);
                        })
                      }}
                    />
                  </Form.Item>
                </Space>
              </Space>
              {xAxisCode && <div>
                <Controlled
                  value={this.state[`${xAxisCodeType}Tmp`] || ''}
                  options={CodeOptions}
                  onBeforeChange={(cm, data, value) => {
                    let state = {};
                    state[`${xAxisCodeType}Tmp`] = value;
                    this.setState(state);
                  }}
                  onKeyUp={(cm, event) => {
                    if ((/^[a-z$._]$/ig).test(event.key)) {
                      showHint(cm, CodeMirror.hint.javascript, { "globalScope": window });
                    }
                  }}
                /></div>}
            </TabPane>}
          {!ChartTypes[chartTypeSelected].xyAxisExclude &&
            <TabPane tab="yAxis" key="Tab-yAxis">
              <Space className="d-flex flex-wrap align-content-start">
                <Space className="d-flex flex-wrap">
                  <Form.Item label="name" className="config-item">
                    <Input title={yAxisName} className="config-input"
                      value={yAxisName}
                      onChange={(e) => {
                        this.setState({ yAxisName: e.target.value }, () => {
                          run(this.state);
                        })
                      }} />
                  </Form.Item>
                  <Form.Item label="type" className="config-item">
                    <Select {...SelectProps} allowClear={true}
                      className="config-input" placeholder="Select type"
                      value={yAxisType} onChange={(value) => {
                        if (value === undefined) {
                          return;
                        }
                        this.setState({
                          yAxisType: value,
                          yAxisFormatter: TypeObj[value].formatter,
                          yAxisMin: TypeObj[value].min,
                          yAxisMax: TypeObj[value].max,
                          yAxisFormatterTmp: TypeObj[value].formatter,
                          yAxisMinTmp: TypeObj[value].min,
                          yAxisMaxTmp: TypeObj[value].max,
                        }, () => {
                          run(this.state)
                        })
                      }} onClear={() => {
                        this.setState({
                          yAxisType: undefined,
                          yAxisFormatter: undefined,
                          yAxisMin: undefined,
                          yAxisMax: undefined,
                          yAxisFormatterTmp: undefined,
                          yAxisMinTmp: undefined,
                          yAxisMaxTmp: undefined,
                        }, () => {
                          run(this.state)
                        })
                      }}>
                      {
                        _.map(Types, (type, index) => {
                          return <Option title={type} key={type} value={type} >
                            {TypeObj[type].optionLabel}
                          </Option>
                        })
                      }
                    </Select>
                  </Form.Item>
                  <Form.Item label={<span className="code-expand-icon code-box-code-action" onClick={() => {
                    this.setState({ yAxisCode: !yAxisCode }, () => {
                      const { yAxisCode, yAxisCodeType } = this.state;
                      if (!yAxisCode) {
                        this.testFunc(yAxisCodeType || _.keys(YFuncTypes)[0]);
                      }
                    })
                  }} >
                    {yAxisCode ?
                      <img title="shrink code" alt="shrink code" src="/svg/open.svg" className="code-expand-icon-show" />
                      :
                      <img title="expand code" alt="expand code" src="/svg/close.svg" className="code-expand-icon-hide" />
                    }
                  </span>} className="config-item">
                    {yAxisCode && <Select {...SelectProps}
                      className="config-input" placeholder="Select type"
                      value={yAxisCodeType} onChange={(value, option) => {
                        this.setState({ yAxisCodeType: value })
                      }}>
                      {
                        _.map(_.values(YFuncTypes), (funcType, index) => {
                          return <Option title={funcType.type} key={funcType.type} value={funcType.type} >
                            {funcType.optionLabel}
                          </Option>
                        })
                      }
                    </Select>}
                  </Form.Item>
                  <Form.Item label="gap" className="config-item">
                    <InputNumber
                      className="config-input"
                      min={-200}
                      max={200}
                      value={yAxisNameGap || DefaultChartConfig.yAxisNameGap}
                      formatter={value => `${value}px`}
                      parser={value => value.replace('px', '')}
                      onChange={(value) => {
                        this.setState({ yAxisNameGap: value }, () => {
                          run(this.state);
                        })
                      }}
                    />
                  </Form.Item>
                  <Form.Item label="label rotate" className="config-item">
                    <InputNumber
                      className="config-input"
                      value={yAxisLabelRotate}
                      formatter={value => `${value}°`}
                      parser={value => value.replace('°', '')}
                      onChange={(value) => {
                        this.setState({ yAxisLabelRotate: value }, () => {
                          run(this.state);
                        })
                      }}
                    />
                  </Form.Item>
                </Space>
              </Space>
              {yAxisCode && <div>
                <Controlled
                  value={this.state[`${yAxisCodeType}Tmp`] || ''}
                  options={CodeOptions}
                  onBeforeChange={(cm, data, value) => {
                    let state = {};
                    state[`${yAxisCodeType}Tmp`] = value;
                    this.setState(state);
                  }}
                  onKeyUp={(cm, event) => {
                    if ((/^[a-z$._]$/ig).test(event.key)) {
                      showHint(cm, CodeMirror.hint.javascript, { "globalScope": window });
                    }
                  }}
                /></div>}
            </TabPane>}
          <TabPane tab="other" key="Tab-other">
            <Space className="d-flex flex-wrap align-content-start">
              <Form.Item label="theme" className="config-item">
                <Select {...SelectProps}
                  className="config-input" placeholder="theme"
                  value={theme} onChange={(value, option) => {
                    this.setState({ theme: value }, () => {
                      run(this.state)
                    })
                  }} >
                  {
                    _.map(Themes, (theme, index) => {
                      return <Option title={theme} key={theme}
                        value={theme} >{theme}</Option>
                    })
                  }
                </Select>
              </Form.Item>
              <Form.Item label="name location" className="config-item">
                <Select {...SelectProps}
                  className="config-input" placeholder="name location"
                  value={nameLocation} onChange={(value, option) => {
                    this.setState(_.cloneDeep(DefaultNameLocationConfig[value]), () => {
                      run(this.state)
                    })
                  }} >
                  {
                    _.map(_.keys(DefaultNameLocationConfig), (nameLocation, index) => {
                      return <Option title={nameLocation} key={nameLocation}
                        value={nameLocation} >{nameLocation}</Option>
                    })
                  }
                </Select>
              </Form.Item>
              <Form.Item label="data zoom" className="config-item">
                <Switch checked={dataZoom} onChange={(checked) => {
                  this.setState({ dataZoom: checked }, () => {
                    run(this.state);
                  });
                }} />
              </Form.Item>
              <Form.Item label="sort" className="config-item">
                <Cascader getPopupContainer={SelectProps.getPopupContainer}
                  className="config-input"
                  value={sortOption || DefaultChartConfig.sortOption}
                  options={properties.length > axis.length ?
                    _.reduce(properties.slice(axis.length), (prev, curr, index) => {
                      prev.push({
                        value: 'yAxis_' + (index + axis.length),
                        label: 'yAxis_' + (index + axis.length),
                      })
                      return prev;
                    }, _.cloneDeep(SortOptions)) : SortOptions}
                  onChange={this.onChangeSort}
                />
              </Form.Item>
              <Form.Item label="scale" className="config-item">
                <Switch checked={scale} onChange={(checked) => {
                  this.setState({ scale: checked }, () => {
                    run(this.state);
                  });
                }} />
              </Form.Item>
              <Form.Item label={`font scale`} className="config-item">
                <Slider style={{ width: "200px" }}
                  step={(90 / 30 - 10 / 30) / 100} min={10 / 30}
                  max={90 / 30} value={fontScale || DefaultChartConfig.fontScale}
                  tipFormatter={(value) => {
                    return `${parseInt(100 * value / (DefaultChartConfig.fontScale))}%`;
                  }}
                  onChange={(value) => {
                    this.setState({ fontScale: value }, () => {
                      run(this.state);
                    });
                  }}
                />
              </Form.Item>
            </Space>
            <Form.Item label={<b>grid</b>} colon={false} className="config-item">
              <Space className="d-flex flex-wrap align-content-start">
                {_.map(_.keys(DefaultChartConfig.grid), (direct, index, arr) => {
                  return <Form.Item key={direct} label={`${direct}`} className="config-item">
                    <InputNumber
                      min={-200}
                      max={200}
                      value={(grid && grid[direct] || DefaultChartConfig.grid[direct]).replace('%', '')}
                      formatter={value => `${value}%`}
                      parser={value => value.replace('%', '')}
                      onChange={(value) => {
                        let obj = {};
                        obj[direct] = value + "%";
                        this.setState({ grid: _.assign(grid || {}, obj) }, () => {
                          run(this.state);
                        })
                      }}
                    />
                  </Form.Item>
                })}
              </Space>
            </Form.Item>
            <Form.Item label={<b>title grid</b>} colon={false} className="config-item">
              <Space className="d-flex flex-wrap align-content-start">
                {_.map(_.keys(DefaultChartConfig.titleGrid), (direct, index, arr) => {
                  return <Form.Item key={direct} label={`${direct}`} className="config-item">
                    <InputNumber
                      min={-50}
                      max={150}
                      value={(titleGrid && titleGrid[direct] || DefaultChartConfig.titleGrid[direct]).replace('%', '')}
                      formatter={value => `${value}%`}
                      parser={value => value.replace('%', '')}
                      onChange={(value) => {
                        let obj = {};
                        obj[direct] = value + "%";
                        this.setState({ titleGrid: _.assign(titleGrid || DefaultChartConfig.titleGrid, obj) }, () => {
                          run(this.state);
                        })
                      }}
                    />
                  </Form.Item>
                })}
              </Space>
            </Form.Item>
          </TabPane>
          <TabPane tab="data" key="Tab-data">
            <RenderObject obj={datas}></RenderObject>
          </TabPane>
          <TabPane tab="table" key="Tab-table">
            <Table dataSource={datas}  >
              {
                _.map(properties, (key, index) => {
                  return <Column title={key} dataIndex={key} key={key} />
                })
              }
            </Table>
          </TabPane>
        </Tabs >
      </div >
    );
  }
}

class RenderObject extends React.Component {
  static propTypes = {
    obj: PropTypes.array
  };

  static defaultProps = {};
  constructor(props) {
    super(props);
    this.ref = React.createRef();
  }

  componentDidMount() {
    const { obj } = this.props;
    showObject(obj, this.ref.current);
  }

  render() {
    const { obj } = this.props;
    const menu = (
      <Menu onClick={async (info) => {
        switch (info.key) {
          case "copySource":
            navigator.permissions.query({ name: "clipboard-write" }).then(result => {
              if (result.state === "granted" || result.state === "prompt") {
                /* write to the clipboard now */
                navigator.clipboard.writeText(JSON.stringify(obj)).then(function () {
                  showToast("copy success!")
                }, function () {
                  showToast("copy failed!")
                });
              } else {
                showToast("no right to write clipboard!")
              }
            }).catch((err) => {
              console.error(err);
            });
            break;
        }
      }}>
        <Menu.Item key="copySource" >copy source</Menu.Item>
      </Menu>
    );
    return <Dropdown overlay={menu} trigger={['contextMenu']}>
      <div className="code-edit-result"  >
        <div className="data">
          <div ref={this.ref}>
          </div>
        </div>
      </div>
    </Dropdown>
  }
}