const Data = {
    blocks: [
        {
            type: "header",
            data: {
                text: "Editor.js",
                level: 2
            }
        }
    ]
};

export function imageDataFunc(url, caption) {
    return {
        blocks: [
            {
                type: "simpleImage",
                data: {
                    url: url,
                    caption: caption,
                    withBorder: true,
                    withBackground: true,
                    stretched: false
                }
            }
        ]
    };
}
export const DefaultData = JSON.stringify(Data);