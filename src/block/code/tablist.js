// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: http://codemirror.net/LICENSE

var CodeMirror = require("codemirror");

CodeMirror.commands.tabAndIndentMarkdownList = function (cm) {
    cm.replaceSelection("");
};

CodeMirror.commands.shiftTabAndUnindentMarkdownList = function (cm) {
    cm.replaceSelection("");
};
