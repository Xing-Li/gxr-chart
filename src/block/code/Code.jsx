import { CopyOutlined, PlusOutlined } from '@ant-design/icons';
import { Menu } from 'antd';
import 'codemirror';
import CodeMirror from 'codemirror';
import 'codemirror/addon/comment/comment';
import 'codemirror/addon/display/placeholder.js';
import 'codemirror/addon/hint/show-hint';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import { Controlled } from 'react-codemirror2';
import showHint from '../../util/CodeMirror.showHint.js';
import Chart from "../chart";
require('codemirror/addon/hint/javascript-hint');
require('codemirror/addon/fold/foldcode');
require('codemirror/addon/fold/foldgutter');
require('codemirror/addon/fold/comment-fold');
require("./tablist");
require('codemirror/mode/javascript/javascript');
const { CodeTools, showToast } = require('../../util/utils');
const { showWrapperObject } = require('../../util/helpers');

const CodeOptions = {
    mode: 'javascript',
    theme: 'base16-light',
    lineNumbers: false,
    lineWrapping: false,
    viewportMargin: Infinity,
    styleSelectedText: true,
    matchBrackets: false,
    styleActiveLine: false,
    nonEmpty: false,
    // foldGutter: false,
    placeholder: "Type,then shift-enter to run",
    // gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
};
const debug = false;
export default class Code extends React.Component {
    static propTypes = {
        readOnly: PropTypes.bool,
        data: PropTypes.shape({
            value: PropTypes.string,
            openEdit: PropTypes.bool,
            runCodeResult: PropTypes.string,
            pinCode: PropTypes.bool,
        }),
        api: PropTypes.object,
        saveData: PropTypes.func,
        settingWrapper: PropTypes.object,
        shareData: PropTypes.object,
        saveShareData: PropTypes.func,
    };

    static defaultProps = {};
    constructor(props) {
        super(props);
        this.state = _.assign({}, this.props.data, {
            shareState: this.props.shareData,
            readOnly: this.props.readOnly
        });
        this.ref = React.createRef();
        this.domRef = React.createRef();
        this.cacheState = undefined;
        this.addObj = undefined;
    }

    componentDidMount() {
        const { openEdit, pinCode, needRun } = this.state;
        const { settingWrapper } = this.props;
        let pinCodeBtn = settingWrapper.querySelector(".pin-code");
        if (pinCode) {
            pinCodeBtn.classList.add("btn-pin");
            pinCodeBtn.classList.remove("btn-unpin");
            this.cm && this.cm.focus();
        } else {
            pinCodeBtn.classList.remove("btn-pin");
            pinCodeBtn.classList.add("btn-unpin");
        }
        let editCodeBtn = settingWrapper.querySelector(".edit-code");
        if (openEdit) {
            editCodeBtn.classList.add("btn-fill");
            editCodeBtn.classList.remove("btn-no-fill");
            this.cm && this.cm.focus();
        } else {
            editCodeBtn.classList.remove("btn-fill");
            editCodeBtn.classList.add("btn-no-fill");
        }
        let runCodeBtn = settingWrapper.querySelector(".run-code");
        if (needRun) {
            runCodeBtn.classList.add("btn-fill");
            runCodeBtn.classList.remove("btn-no-fill");
        } else {
            runCodeBtn.classList.remove("btn-fill");
            runCodeBtn.classList.add("btn-no-fill");
        }
    }

    componentWillUnmount() {
        CodeTools.splice(CodeTools.indexOf(this), 1);
    }

    componentDidUpdate(prevProps, prevState) {
        const { openEdit, pinCode, value, runCodeResult, needRun, shareState } = this.state;
        const { settingWrapper, saveData, shareData, saveShareData, readOnly } = this.props;
        if (readOnly !== this.state.readOnly) {
            this.setState({ readOnly: readOnly });
        }
        if (!_.isEqual(shareState, prevState.shareState)) {
            debug && console.log(JSON.stringify(shareState) + " <- " + JSON.stringify(prevState.shareState))
            this.run(value, true);
        }
        if (pinCode !== prevState.pinCode) {
            let pinCodeBtn = settingWrapper.querySelector(".pin-code");
            if (pinCode) {
                pinCodeBtn.classList.add("btn-pin");
                pinCodeBtn.classList.remove("btn-unpin");
            } else {
                pinCodeBtn.classList.remove("btn-pin");
                pinCodeBtn.classList.add("btn-unpin");
            }
        }
        if (openEdit !== prevState.openEdit) {
            let editCodeBtn = settingWrapper.querySelector(".edit-code");
            if (openEdit) {
                editCodeBtn.classList.add("btn-fill");
                editCodeBtn.classList.remove("btn-no-fill");
                this.cm && this.cm.focus();
            } else {
                editCodeBtn.classList.remove("btn-fill");
                editCodeBtn.classList.add("btn-no-fill");
            }
        }
        if (needRun !== prevState.needRun) {
            let runCodeBtn = settingWrapper.querySelector(".run-code");
            if (needRun) {
                runCodeBtn.classList.add("btn-fill");
                runCodeBtn.classList.remove("btn-no-fill");
            } else {
                runCodeBtn.classList.remove("btn-fill");
                runCodeBtn.classList.add("btn-no-fill");
            }
        }
        if (runCodeResult !== prevState.runCodeResult) {
            let parent = this.ref.current;
            let data = parent.querySelector(":scope > .data");
            showWrapperObject(runCodeResult, data, this.isDeclare(value))
        }
        if (!_.isEqual(prevState, this.state)) {
            let dataTmp = _.cloneDeep(this.state);
            delete dataTmp.shareState;
            delete dataTmp.readOnly;
            saveData(dataTmp);
        }
    }

    editCode = () => {
        const { settingWrapper } = this.props;
        let button = settingWrapper.querySelector(".edit-code");
        let needOpenEdit = button.classList.contains("btn-no-fill");
        this.setState({ openEdit: needOpenEdit }, () => {
            const { openEdit } = this.state;
            !openEdit && this.runCode();
        })
    }

    runCode = () => {
        const { value } = this.state;
        const { settingWrapper } = this.props;
        let button = settingWrapper.querySelector(".run-code");
        let needRunCode = button.classList.contains("btn-fill");
        if (needRunCode) {
            this.run(value);
        } else {
            this.focusResult();
        }
    }

    pinCode = () => {
        const { pinCode } = this.state;
        const { settingWrapper } = this.props;
        let button = settingWrapper.querySelector(".pin-code");
        let needpin = button.classList.contains("btn-unpin");
        this.setState({ pinCode: needpin });
    }

    delete = (event) => {
        const { api } = this.props;
        if (api.blocks.getCurrentBlockIndex() === -1) {
            return;
        }
        let block = api.blocks.getBlockByIndex(api.blocks.getCurrentBlockIndex());
        if (block && block.name === 'codeTool' && block.holder && block.holder.contains(this.domRef.current)) {
            api.blocks.delete(api.blocks.getCurrentBlockIndex());
            api.toolbar.close();
            api.tooltip.hide();
            event && event.stopPropagation();
            return;
        }
    }

    copyResult = (event) => {
        const { runCodeResult } = this.state;
        navigator.permissions.query({ name: "clipboard-write" }).then(async result => {
            if (result.state === "granted" || result.state === "prompt") {
                let content;
                switch (typeof runCodeResult) {
                    case "object":
                        content = JSON.stringify(runCodeResult, null, 2);
                        break;
                    default:
                        content = runCodeResult;
                }
                /* write to the clipboard now */
                navigator.clipboard.writeText(content).then(function () {
                    showToast("copy success!")
                }, function () {
                    showToast("copy failed!")
                });
            } else {
                showToast("no right to write clipboard!")
            }
        }).catch((err) => {
            console.error(err);
        });
    }

    drawChart = () => {
        const { api } = this.props;
        if (api.blocks.getCurrentBlockIndex() === -1) {
            return;
        }
        let index = api.blocks.getCurrentBlockIndex();
        api.blocks.insert("chart", {
            class: Chart,
            contentEditable: false
        }, undefined, index + 1, true);
    }

    isDeclare = (text) => {
        return /^[A-Za-z_]{1,}[_\w]{0,}(\s+)?=(\s+)?/.test(text.trim());
    }

    run = (text, checkSame) => {
        let self = this;
        try {
            if (this.isDeclare(text)) {
                if (checkSame) {
                    return;
                }
                let addObjTmp;
                let code = `addObjTmp = {${text.replace('=', ':')}}`;
                let ret = eval(code);
                let shareStateTmp = _.assign({}, _.cloneDeep(this.state.shareState), addObjTmp);
                if (this.addObj) {
                    _.each(_.keys(this.addObj), (key) => {
                        if (!addObjTmp[key])
                            delete shareStateTmp[key];
                    })
                }
                if (!_.isEqual(shareStateTmp, this.state.shareState)) {
                    debug && console.log("---" + JSON.stringify(this.state.shareState));
                    this.setState({ shareState: shareStateTmp }, () => {
                        this.addObj = addObjTmp;
                        const { saveShareData } = this.props;
                        const { shareState } = this.state;
                        saveShareData(shareState);
                    })
                }
                self.setState({ needRun: false, runCodeResult: ret }, () => {
                    this.focusResult();
                })
                return;
            }
            debug && console.log("..." + JSON.stringify(this.state.shareState));
            // let reg = /(\b|\W+)api(\b|\W+)/g
            let cacheStateTmp = {};
            _.keys(this.state.shareState).forEach((key, index, arr) => {
                let reg = new RegExp(`(\\b|\\W+)${key}(\\b|\\W+)`, 'g');
                if (reg.test(text)) {
                    cacheStateTmp[key] = this.state.shareState[key];
                }
            })
            if (checkSame && _.isEqual(cacheStateTmp, this.cacheState)) {
                return;
            }
            this.cacheState = cacheStateTmp;
            // let retPromise = ((new AsyncFunction(text)).bind(this))();
            let string = `(async ()=>{
                ${_.keys(this.cacheState).map((key) => {
                return `let ${key} = this.state.shareState.${key};`
            }).join("\n")} 
              ${text}})()`;
            debug && console.log(string)
            let retPromise = eval(string)
            retPromise.then(ret => {
                self.setState({ needRun: false, runCodeResult: ret }, () => {
                    this.focusResult();
                })
            }).catch((err) => {
                self.setState({ runCodeResult: `&nbsp;${err.message + ""}` }, () => {
                    this.focusResult(true);
                })
                console.error(err);
            });
        } catch (err) {
            self.setState({ runCodeResult: `&nbsp;${err.message + ""}` }, () => {
                this.focusResult(true);
            })
            console.error(err);
        }
    }

    focusResult = (err) => {
        this.ref.current.style.borderLeft = `8px solid ${err ? "var(--error-color)" : "transparent"}`;
        setTimeout(() => {
            this.ref.current.style.borderLeft = `8px solid var(--warning-color)`;
            setTimeout(() => {
                this.ref.current.style.borderLeft = `8px solid ${err ? "var(--error-color)" : "transparent"}`;
            }, 200)
        }, 200);
    }

    DisplayResult = () => {
        const { runCodeResult } = this.state;
        if (!runCodeResult) {
            return <span dangerouslySetInnerHTML={{ __html: `&nbsp;${runCodeResult + ""}` }}></span>
        } else {
            return <span dangerouslySetInnerHTML={{ __html: `&nbsp;${runCodeResult}` }}></span>
        }
    }

    render() {
        const { saveData, api } = this.props;
        const { value, openEdit, pinCode, runCodeResult, needRun, readOnly } = this.state;
        return <div ref={this.domRef} contentEditable={false}
            className={`code-edit ${openEdit ? "edit" : ""}`}
            onBlur={() => {
                openEdit && this.editCode()
            }}>
            <div className="code-edit-menus">
                <div className="code-edit-menus-menu">
                    <EditCodeIcon fill={openEdit} onMouseUp={this.editCode} />
                </div>
                <Menu>
                    <Menu.Item onMouseOver={(event) => {
                        api.tooltip.show(event.currentTarget, `${pinCode ? "Pin" : "Unpin"} Code`, { placement: "right" });
                    }} onMouseLeave={(event) => {
                        api.tooltip.hide()
                    }} onMouseUp={this.pinCode} key="pinCode"><PinCodeIcon pin={pinCode} /></Menu.Item>
                    <Menu.Item onMouseOver={(event) => {
                        api.tooltip.show(event.currentTarget, `${needRun ? "Run" : "Refresh"} Code`, { placement: "right" });
                    }} onMouseLeave={(event) => {
                        api.tooltip.hide()
                    }} onMouseUp={this.runCode} key="runCode"><RunCodeIcon fill={needRun} /></Menu.Item>
                    <Menu.Item onMouseOver={(event) => {
                        api.tooltip.show(event.currentTarget, `Delete Block`, { placement: "right" });
                    }} onMouseLeave={(event) => {
                        api.tooltip.hide()
                    }} onMouseUp={this.delete} key="delete"><DeleteIcon /></Menu.Item>
                    <Menu.Item onMouseOver={(event) => {
                        api.tooltip.show(event.currentTarget, `Copy Result`, { placement: "right" });
                    }} onMouseLeave={(event) => {
                        api.tooltip.hide()
                    }} onMouseUp={this.copyResult} key="copyResult"><CopyOutlined /></Menu.Item>
                    {/* <Menu.Item disabled={!(this.isDeclare(value) || runCodeResult instanceof Array)} onMouseUp={
                        (this.isDeclare(value) || runCodeResult instanceof Array) ?
                            this.drawChart : (() => { })} key="drawChart"><DrawChartIcon /></Menu.Item> */}
                </Menu>
            </div>
            <div className="code-edit-add-up" onMouseOver={(event) => {
                api.tooltip.show(event.currentTarget, `Insert New Block Up`, { placement: "right" });
            }} onMouseLeave={(event) => {
                api.tooltip.hide()
            }} onMouseUp={() => {
                let index = api.blocks.getCurrentBlockIndex();
                api.blocks.insert(undefined, undefined, undefined, index >= 0 ? index : 0, true);
            }}><PlusOutlined></PlusOutlined></div>
            <div className="code-edit-add-down" onMouseOver={(event) => {
                api.tooltip.show(event.currentTarget, `Insert New Block Down`, { placement: "right" });
            }} onMouseLeave={(event) => {
                api.tooltip.hide()
            }} onMouseUp={() => {
                let index = api.blocks.getCurrentBlockIndex();
                api.blocks.insert(undefined, undefined, undefined, index + 1, true);
            }}><PlusOutlined></PlusOutlined></div>
            <div className="code-edit-content">
                <div className="code-edit-result" ref={this.ref}><div className="data">&nbsp;</div></div>

                {
                    (openEdit || pinCode) && <div contentEditable={!readOnly}>
                        <Controlled className={`code-edit-cm`}
                            editorDidMount={(cm, value, cb) => {
                                _.each(cm.getWrapperElement().getElementsByClassName("CodeMirror-lines"), (ele) => {
                                    !ele.hasAttribute("contenteditable")
                                        && ele.setAttribute("contenteditable", "true");
                                });
                                this.cm = cm;
                                this.cm.focus();
                            }}
                            onFocus={(cm) => {
                                !openEdit && this.editCode()
                            }}
                            value={value}
                            options={_.assign(CodeOptions, {
                                // readOnly: !openEdit,
                                extraKeys: {
                                    "F10": function (cm) {
                                        cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                                    },
                                    "Esc": (cm) => {
                                        if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                                        this.editCode()
                                    },
                                    'Cmd-/': 'toggleComment',
                                    'Ctrl-/': 'toggleComment',
                                    "Tab": "tabAndIndentMarkdownList",
                                    "Shift-Tab": "shiftTabAndUnindentMarkdownList",
                                    'Shift-Enter': (cm) => {
                                        this.runCode();
                                    },

                                }
                            })}
                            onBeforeChange={(cm, data, value) => {
                                this.setState({ value: value, needRun: true })
                            }}
                            onKeyUp={(cm, event) => {
                                if ((/^[a-z$._]$/ig).test(event.key)) {
                                    showHint(cm, CodeMirror.hint.javascript, { "globalScope": window });
                                }
                            }}
                            onKeyDown={(cm, event) => {
                                if (~['\n', '\r'].indexOf(event.key)) {
                                    _.each(cm.getWrapperElement().getElementsByClassName("CodeMirror-lines"), (ele) => {
                                        !ele.hasAttribute("contenteditable")
                                            && ele.setAttribute("contenteditable", "true");
                                    });
                                }
                            }}
                        />
                    </div>
                }

            </div>
        </div >
    }
}

function PinCodeIcon(props) {
    const { pin, onMouseUp } = props;
    return <span onMouseUp={onMouseUp} role="img" aria-label="pin-code" className={`anticon anticon-pin ${pin ? "btn-pin" : "btn-unpin"}`}>
        <svg width="16" height="16" viewBox="0 0 16 16" fill="currentColor" className="v-top"><path d="M8 1h3v1l-1 1v4l2 .875V9H9v5.125L8 15l-1-.875V9H4V7.875L6 7V3L5 2V1z"></path></svg>
    </span>
}

function EditCodeIcon(props) {
    const { fill, onMouseUp } = props;
    return <span onMouseUp={onMouseUp} title={`${fill ? "Edit" : "Exit Edit"} Code`} role="img" aria-label="edit-code" className={`anticon anticon-edit-code ${fill ? "btn-fill" : "btn-no-fill"}`}>
        <svg width="16" height="16" viewBox="0 0 16 16"><path d="M9.23628 2L12.9259 5.69224L6.47735 12.9933L1.91889 14.1539L2.78769 9.3011L9.23628 2Z" stroke="currentColor" strokeWidth="2" strokeLinejoin="round"></path></svg>
    </span>
}

function RunCodeIcon(props) {
    const { fill, onMouseUp } = props;
    return <span onMouseUp={onMouseUp} role="img" aria-label="run-code" className={`anticon anticon-run-code ${fill ? "btn-fill" : "btn-no-fill"}`}>
        <svg width="16" height="16" strokeLinejoin="round"><path d="M11.7206 6.94335C12.2406 7.34365 12.2406 8.12786 11.7206 8.52816L5.60999 13.2321C4.95242 13.7383 4 13.2696 4 12.4397L4 3.03178C4 2.20194 4.95243 1.73318 5.60999 2.23937L11.7206 6.94335Z" stroke="currentColor" strokeWidth="1.6"></path></svg>
    </span>
}

function DeleteIcon(props) {
    const { onMouseUp } = props;
    return <span onMouseUp={onMouseUp} role="img" aria-label="run-code" className={`anticon anticon-delete`}>
        <svg width="16" height="16" viewBox="0 0 16 16" fill="none"><path d="M5 3V5H11V3L5 3ZM3 3V5H1V7H3V13C3 14.1046 3.89543 15 5 15H11C12.1046 15 13 14.1046 13 13V7H15V5H13V3C13 1.89543 12.1046 1 11 1H5C3.89543 1 3 1.89543 3 3ZM5 13V7H7V13H5ZM9 13H11V7H9L9 13Z" fill="currentColor" strokeWidth="2" fillRule="evenodd" clipRule="evenodd"></path></svg>
    </span>
}

function DrawChartIcon(props) {
    const { onMouseUp } = props;
    return <span onMouseUp={onMouseUp} title={`Draw Chart`} role="img" aria-label="run-code" className={`anticon anticon-draw-chart`}>
        <svg viewBox="64 64 896 896" focusable="false" data-icon="area-chart" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M888 792H200V168c0-4.4-3.6-8-8-8h-56c-4.4 0-8 3.6-8 8v688c0 4.4 3.6 8 8 8h752c4.4 0 8-3.6 8-8v-56c0-4.4-3.6-8-8-8zm-616-64h536c4.4 0 8-3.6 8-8V284c0-7.2-8.7-10.7-13.7-5.7L592 488.6l-125.4-124a8.03 8.03 0 00-11.3 0l-189 189.6a7.87 7.87 0 00-2.3 5.6V720c0 4.4 3.6 8 8 8z"></path></svg>
    </span>
}