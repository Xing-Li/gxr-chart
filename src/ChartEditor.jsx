import {
    ArrowDownOutlined, ArrowUpOutlined, DeleteOutlined, EditOutlined, FormOutlined,
    SyncOutlined
} from '@ant-design/icons';
import { Button, Drawer, Form, Radio } from 'antd';
import cx from "classnames";
import _ from 'lodash';
import PropTypes from "prop-types";
import React from "react";
import ChartSetting from './ChartSetting';
import { chalk, essos, westeros, wonderland } from './theme';
import {
    ChartTypes, DefaultChartConfig,
    Themes, transfor, transforFormatter, transforMX
} from "./util/utils";
let echarts = require('echarts/lib/echarts');
require('echarts/lib/chart/line');
require('echarts/lib/chart/lines');
require('echarts/lib/chart/bar');
require('echarts/lib/chart/pie');
require('echarts/lib/chart/scatter');
require('echarts/lib/component/tooltip');
require('echarts/lib/component/title');
require('echarts/extension/dataTool');
require(`echarts/theme/eduardo`);
_.each(Themes.slice(1), (theme) => {
    require(`echarts/theme/${theme}`);
})

const AddThemes = ['chalk', 'essos', 'westeros', 'wonderland']
Themes.push(...AddThemes);
Themes.sort();
echarts.registerTheme('chalk', chalk)
echarts.registerTheme('essos', essos)
echarts.registerTheme('westeros', westeros)
echarts.registerTheme('wonderland', wonderland)
export default class ChartEditor extends React.Component {
    static propTypes = {
        //wrapper element of this
        wrapper: PropTypes.object,
        data: PropTypes.object,
        saveData: PropTypes.func,
        api: PropTypes.object
    };
    static defaultProps = {};
    constructor(props) {
        super(props)
        const { data } = this.props;
        this.state = data;
        this.settingRef = React.createRef();
    }

    showSettings = () => {
        this.setState({
            visible: true,
        });
    };

    onClose = () => {
        this.setState({
            visible: false,
        });
    };

    onChange = e => {
        this.setState({
            placement: e.target.value,
        });
    };

    componentDidMount() {
        _.each(this.sortChartConfigs(), (chartConfig, index) => {
            this.drawChart(chartConfig);
        })
    }

    componentDidUpdate(prevProps, prevState) {
    }

    componentWillUnmount() {
        document.getElementById("drawer").innerHTML = ""
    }

    run = (chartConfig) => {
        this.drawChart(chartConfig)
    }

    drawChart = (chartConfig) => {
        const { wrapper } = this.props;
        transfor(chartConfig);
        const { category, propertyMap, chartTypeSelected,
            source, chartKey, width, height, title, titleGrid,
            dataZoom, nameLocation, grid,
            xAxisType, yAxisType, xAxisName, yAxisName,
            xAxisNameGap, yAxisNameGap, xAxisLabelRotate, yAxisLabelRotate,
            xAxisFormatter, yAxisFormatter, xAxisMin, yAxisMin, xAxisMax, yAxisMax,
            fontScale, chartTypes, scale,
        } = chartConfig;
        let theme = chartConfig.theme || Themes[1];
        const ele = wrapper.querySelector(".main").querySelector(`.${chartKey}`);
        ele.style.width = window.innerWidth * width * 0.01 + "px";
        ele.style.height = window.innerHeight * height * 0.01 + "px";
        if (category === undefined) {
            return;
        }
        const properties = _.keys(propertyMap);
        if (!properties.length) {
            return;
        }
        if (properties.length < ChartTypes[chartTypeSelected].axis.length) {
            return;
        }
        let myChart;
        if (ele.myChart) {
            myChart = ele.myChart;
            if (chartConfig.oldTheme === theme) {
                myChart.clear();
                myChart.resize({
                    width: window.innerWidth * width * 0.01 + "px",
                    height: window.innerHeight * height * 0.01 + "px",
                })
            } else {
                myChart.dispose();
                delete ele.myChart;
            }
        }
        if (!ele.myChart) {
            myChart = echarts.init(ele, theme, _.assign({
                width: window.innerWidth * width * 0.01 + "px",
                height: window.innerHeight * height * 0.01 + "px",
            }, {
            }));
            chartConfig.oldTheme = theme;
            ele.myChart = myChart;
        }
        let option = {}
        if (!ChartTypes[chartTypeSelected].xyAxisExclude) {
            _.assign(option, {
                xAxis: _.assign({
                    type: xAxisType,
                    name: xAxisName,
                    nameLocation: nameLocation || DefaultChartConfig.nameLocation,
                    nameGap: xAxisNameGap || DefaultChartConfig.xAxisNameGap,
                    nameTextStyle: {
                        color: "black",
                        fontWeight: "lighter",
                        fontSize: 20 * fontScale
                    },
                    axisLabel: _.assign(
                        xAxisFormatter ? {
                            formatter: transforFormatter(xAxisFormatter)
                        } : {},
                        {
                            rotate: xAxisLabelRotate || 0,
                            fontSize: 12 * fontScale
                        }),
                    scale: scale
                },
                    xAxisMin ? { min: transforMX(xAxisMin) } : {},
                    xAxisMax ? { max: transforMX(xAxisMax) } : {}),
                yAxis: _.assign({
                    type: yAxisType,
                    name: yAxisName,
                    nameLocation: nameLocation || DefaultChartConfig.nameLocation,
                    nameGap: yAxisNameGap || DefaultChartConfig.yAxisNameGap,
                    nameTextStyle: {
                        color: "black",
                        fontWeight: "lighter",
                        fontSize: 20 * fontScale
                    },
                    axisLabel: _.assign(
                        yAxisFormatter ? {
                            formatter: transforFormatter(yAxisFormatter)
                        } : {},
                        {
                            rotate: yAxisLabelRotate || 0,
                            fontSize: 12 * fontScale
                        }),
                    scale: scale
                },
                    yAxisMin ? { min: transforMX(yAxisMin) } : {},
                    yAxisMax ? { max: transforMX(yAxisMax) } : {}),
            })
            if (dataZoom) {
                _.assign(option, {
                    dataZoom: [
                        {
                            type: 'slider',
                            xAxisIndex: 0,
                            start: 0,
                            end: 100
                        },
                        {
                            type: 'inside',
                            xAxisIndex: 0,
                            start: 0,
                            end: 100
                        },
                        {
                            type: 'slider',
                            yAxisIndex: 0,
                            start: 0,
                            end: 100
                        },
                        {
                            type: 'inside',
                            yAxisIndex: 0,
                            start: 0,
                            end: 100
                        }
                    ],
                })
            }
        }
        let series = _.reduce(properties.length > 1 ? properties.slice(1) : [], (prev, curr, index) => {
            prev.push(_.assign({ type: _.isEmpty(chartTypes[1 + index]) ? chartTypeSelected : chartTypes[1 + index] }, {}))
            return prev
        }, []);
        let mergeSource = [[...properties]]
        mergeSource.splice(1, 0, ...source);
        myChart.setOption(_.assign(option, {
            grid: grid || DefaultChartConfig.grid,
            title: {
                text: title || DefaultChartConfig.title,
                // subtext: properties.join("-"),
                left: (titleGrid || DefaultChartConfig.titleGrid).left,
                top: (titleGrid || DefaultChartConfig.titleGrid).top,
                textStyle: {
                    fontSize: 30 * fontScale
                },
                subtextStyle: {
                    fontSize: 20 * fontScale
                }
            },
            legend: { left: "0%", show: false },
            tooltip: {},
            dataset: {
                dimensions: properties,
                source: mergeSource
            },
            series: series,
        }));
    }

    cancelEdit = () => {
        this.publish(this.settingRef.current.oldChartConfig);
    }

    publishEdit = () => {
        this.publish(this.settingRef.current.state)
    }

    publish = (chartConfig) => {
        this.switchChart(chartConfig, undefined)
    }

    switchChart = (chartConfig, swSelectedKey) => {
        const { chartConfigs, selectedKey } = this.state;
        const { category, propertyMap, chartTypeSelected } = chartConfig;
        const chartKeyTmp = `${category}_${_.keys(propertyMap).join("_")}_${chartTypeSelected}`;
        if (chartConfigs[chartKeyTmp] &&
            chartConfig.createTime !== chartConfigs[chartKeyTmp].createTime &&
            !window.confirm(`Same chart key ${chartKeyTmp}  , overwrite?`)) {
            return;
        }
        delete chartConfigs[selectedKey];
        chartConfig.chartKey = chartKeyTmp;
        chartConfigs[chartKeyTmp] = chartConfig;
        this.setDatas({ chartConfigs: chartConfigs, selectedKey: swSelectedKey })
    }

    deleteChart = (chartKey) => {
        const { chartConfigs, selectedKey } = this.state;
        delete chartConfigs[chartKey];
        let state = { chartConfigs: chartConfigs };
        if (chartKey === selectedKey) _.assign(state, { selectedKey: undefined });
        this.setDatas(state)
    }

    setDatas = (state) => {//save
        const { saveData } = this.props;
        this.setState(state, () => {
            if (state.chartConfigs) {
                _.each(this.sortChartConfigs(), (chartConfig, index) => {
                    this.drawChart(chartConfig);
                });
            }
            saveData(this.state);
        })
    }

    sortChartConfigs = () => {
        const { chartConfigs } = this.state
        return Object.values(chartConfigs).sort((a, b) => {
            return (b.sortIndex !== undefined && a.sortIndex !== undefined && a.sortIndex - b.sortIndex)
                || b.createTime - a.createTime;
        })
    }

    moveUp = (event, chartKey) => {
        const { chartConfigs } = this.state;
        let chartConfig = chartConfigs[chartKey];
        let sChartConfigs = this.sortChartConfigs();
        let sortIndex = sChartConfigs.indexOf(chartConfig);
        if (sortIndex > 0) {
            sChartConfigs.forEach((value, index) => {
                value.sortIndex = index;
            })
            let tmp = sChartConfigs[sortIndex].sortIndex;
            sChartConfigs[sortIndex].sortIndex = sChartConfigs[sortIndex - 1].sortIndex;
            sChartConfigs[sortIndex - 1].sortIndex = tmp;
            const { target, clientY } = event;
            this.setState({ chartConfigs: chartConfigs }, () => {
                target.scroll({
                    top: clientY,
                    behavior: 'smooth'
                })
            });
        }
    }

    moveDown = (event, chartKey) => {
        const { chartConfigs } = this.state;
        let chartConfig = chartConfigs[chartKey];
        let sChartConfigs = this.sortChartConfigs();
        let sortIndex = sChartConfigs.indexOf(chartConfig);
        if (sChartConfigs.length - 2 >= 0 && sortIndex <= sChartConfigs.length - 2) {
            sChartConfigs.forEach((value, index) => {
                value.sortIndex = index;
            })
            let tmp = sChartConfigs[sortIndex].sortIndex;
            sChartConfigs[sortIndex].sortIndex = sChartConfigs[sortIndex + 1].sortIndex;
            sChartConfigs[sortIndex + 1].sortIndex = tmp;
            const { target } = event;
            this.setState({ chartConfigs: chartConfigs }, () => {
                window.scroll({
                    top: target.clientHeight,
                    behavior: 'smooth'
                })
            });
        }
    }

    newChart() {
        const { selectedKey, chartConfigs } = this.state;
        if (selectedKey !== DefaultChartConfig.chartKey) {
            let state = { selectedKey: DefaultChartConfig.chartKey };
            if (!chartConfigs[DefaultChartConfig.chartKey]) {
                let chartConfig = _.cloneDeep(DefaultChartConfig);
                chartConfig.createTime = new Date().getTime();
                chartConfigs[chartConfig.chartKey] = chartConfig;
                _.assign(state, { chartConfigs: chartConfigs })
            }
            this.setDatas(state)
        } else {
            let urlEle = document.querySelector(`.chart-div.default`);
            if (urlEle) {
                urlEle.scrollIntoViewIfNeeded(true);
                if (!/(^|\s+)active(\s+|$)/.test(urlEle.className)) {
                    urlEle.className += " active";
                }
                setTimeout(() => {
                    urlEle.className = urlEle.className.replace(/(^|\s+)active(\s+|$)/, ' ');
                }, 500)
            }
        }
    }
    Settings = () => {
        const { visible, placement } = this.state;
        return <Drawer title={"Settings"}
            getContainer={document.getElementById("drawer")}
            destroyOnClose={true}
            placement={placement || "bottom"}
            onClose={this.onClose}
            visible={visible}
            bodyStyle={{ paddingBottom: 12 }}
            footer={
                <div
                    style={{
                        textAlign: 'right',
                    }}
                >
                    <Button onClick={this.onClose} style={{ marginRight: 8 }}>
                        Cancel
                    </Button>
                    <Button onClick={this.onClose} type="primary">
                        Submit
                    </Button>
                </div>
            } >
            <Form.Item label="chart drawer placement" className="config-item">
                <Radio.Group defaultValue={placement || "bottom"} onChange={this.onChange}>
                    <Radio value="top">top</Radio>
                    {/* <Radio value="right">right</Radio> */}
                    <Radio value="bottom">bottom</Radio>
                    {/* <Radio value="left">left</Radio> */}
                </Radio.Group>
            </Form.Item>
        </Drawer>
    }

    ChartSettings = () => {
        const { chartConfigs, selectedKey, placement } = this.state;
        const selectedChartConfig = chartConfigs[selectedKey] || DefaultChartConfig
        return <Drawer
            getContainer={document.getElementById("drawer")}
            destroyOnClose={true}
            className="chart-setting-drawer"
            title={selectedChartConfig.title}
            placement={placement || "bottom"}
            width={320}
            mask={false}
            onClose={this.publishEdit}
            visible={selectedChartConfig !== DefaultChartConfig}
            bodyStyle={{ paddingBottom: 12 }}
            footer={
                <div className="d-flex flex-wrap justify-content-end">
                    <Form.Item label="chart drawer placement" className="config-item">
                        <Radio.Group defaultValue={placement || "bottom"} onChange={this.onChange}>
                            <Radio value="top">top</Radio>
                            <Radio value="right">right</Radio>
                            <Radio value="bottom">bottom</Radio>
                            <Radio value="left">left</Radio>
                        </Radio.Group>
                    </Form.Item>
                    <Button onClick={this.cancelEdit} style={{ marginRight: 8 }}>
                        Cancel
                            </Button>
                    <Button onClick={this.publishEdit} type="primary">
                        Submit
                            </Button>
                </div>
            }
        >
            <div className="d-flex flex-wrap align-content-start">
                <ChartSetting ref={this.settingRef} placement={placement}
                    publish={this.publish} run={this.run}
                    chartConfig={selectedChartConfig}>
                </ChartSetting>
            </div>
        </Drawer>
    }

    render() {
        const { chartConfigs, selectedKey, visible, placement } = this.state;
        const { api } = this.props;
        const sChartConfigs = this.sortChartConfigs();
        return <div className="main chart">
            <div className="d-flex flex-wrap align-content-start chart-list">
                {sChartConfigs.length === 0 && <div>&nbsp;&nbsp;</div>}
                {_.map(sChartConfigs, (value, index, sChartConfigs) => {
                    let editFunc = () => {
                        let selectedKeyTmp = selectedKey !== value.chartKey ? value.chartKey : undefined;
                        selectedKey ?
                            this.switchChart(this.settingRef.current.state, selectedKeyTmp)
                            :
                            this.setDatas({ selectedKey: selectedKeyTmp })
                    }
                    return <React.Fragment key={value.chartKey}>
                        <div key={value.chartKey}
                            className={cx(`chart-div`,
                                { "default": value.chartKey === DefaultChartConfig.chartKey },
                                { "cdx-input": _.keys(value.propertyMap).length < ChartTypes[value.chartTypeSelected].axis.length }
                            )}>
                            <div className="icons">
                                <div className="d-flex flex-row-reverse">
                                    {/* <!-- for editorjs enabled onclick so use mouse up instead--> */}
                                    {
                                        selectedKey === value.chartKey ?
                                            <EditOutlined className="anticon-menu"
                                                onMouseOver={(event) => {
                                                    api.tooltip.show(event.currentTarget, "Toggle publish", { placement: "top" });
                                                }} onMouseLeave={(event) => {
                                                    api.tooltip.hide()
                                                }} onMouseUp={() => {
                                                    this.publishEdit();
                                                }} shape="circle" />
                                            :
                                            <FormOutlined className="anticon-menu"
                                                onMouseOver={(event) => {
                                                    api.tooltip.show(event.currentTarget, "Toggle edit", { placement: "top" });
                                                }} onMouseLeave={(event) => {
                                                    api.tooltip.hide()
                                                }} onMouseUp={editFunc}
                                                shape="circle" />
                                    }
                                    <DeleteOutlined className="anticon-menu"
                                        onMouseOver={(event) => {
                                            api.tooltip.show(event.currentTarget, "Toggle delete", { placement: "top" });
                                        }} onMouseLeave={(event) => {
                                            api.tooltip.hide()
                                        }} onMouseUp={() => {
                                            this.deleteChart(value.chartKey)
                                        }} shape="circle" />
                                    {<ArrowUpOutlined disabled={!(index > 0)}
                                        className={cx("anticon-menu",
                                            { "disabled": !(index > 0) })}
                                        onMouseOver={(event) => {
                                            api.tooltip.show(event.currentTarget, "Toggle move up", { placement: "top" });
                                        }} onMouseLeave={(event) => {
                                            api.tooltip.hide()
                                        }} onMouseUp={(event) => {
                                            this.moveUp(event, value.chartKey);
                                        }} shape="circle" />}
                                    {<ArrowDownOutlined disabled={!(sChartConfigs.length - 2 >= 0 && index <= sChartConfigs.length - 2)}
                                        className={cx("anticon-menu",
                                            { "disabled": !(sChartConfigs.length - 2 >= 0 && index <= sChartConfigs.length - 2) })}
                                        onMouseOver={(event) => {
                                            api.tooltip.show(event.currentTarget, "Toggle move up", { placement: "top" });
                                        }} onMouseLeave={(event) => {
                                            api.tooltip.hide()
                                        }} onMouseUp={(event) => {
                                            this.moveDown(event, value.chartKey);
                                        }} shape="circle" />}
                                    <SyncOutlined className="anticon-menu"
                                        onMouseOver={(event) => {
                                            api.tooltip.show(event.currentTarget, "Toggle update data", { placement: "top" });
                                        }} onMouseLeave={(event) => {
                                            api.tooltip.hide()
                                        }} onMouseUp={() => {
                                        }} shape="circle" />
                                </div>
                            </div>
                            <div className={cx("chart",
                                value.chartKey,
                                { "selected": selectedKey === value.chartKey })} onDoubleClick={editFunc}>
                            </div>
                        </div>
                    </React.Fragment>
                })}
            </div>
            <this.Settings></this.Settings>
            <this.ChartSettings></this.ChartSettings>
        </div >
    }
}