const { ImageCache, isFileImage } = require('../util/helpers.js');
const { getItem, getFileNamesKey } = require("../util/localstorage");

export const fileSeparator = '/';
export function openDirs(fileKey) {
    if (fileKey === null) {
        return;
    }
    let folderKeys = [""];
    let dir = "";
    let arr = fileKey.split(fileSeparator).filter(value => value !== '');
    for (let index = 0; index < arr.length - 1; index++) {
        folderKeys.push(dir += (arr[index] + fileSeparator));
    }
    document.querySelector(".folder").querySelectorAll("details").forEach((details) => {
        if (~folderKeys.indexOf(details.getAttribute("data-link"))) {
            details.setAttribute("open", "");
        }
    })
}

/**
 * 
 * @param {*} uploadUri 
 * @param {*} cloud 
 * @param {*} file { fileKey : ..., type : ... }
 */
export async function getMdFile(uploadUri, cloud, file) {
    let fileKey = file.fileKey;
    let item = getItem(getFileNamesKey() + '.' + fileKey);
    if (item !== undefined) {
        return item;
    }
    if (uploadUri && cloud[fileKey] || isFileImage(file)) {
        if (isFileImage(file) && ImageCache[fileKey]) {
            console.log("image in cache")
            return ImageCache[fileKey];
        }
        let response = await fetch(`${uploadUri}${fileKey}`);
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        let content;
        if (isFileImage(file)) {
            content = URL.createObjectURL(await response.blob());
            ImageCache[fileKey] = content;
        } else {
            content = await response.text();
        }
        return content;
    } else {
        throw new Error(`Can not find the file! ${uploadUri}${fileKey}`)
    }
}

export function pasteScreenShot(processFunc) {
    // window.addEventListener('paste', ... or
    document.onpaste = function (event) {
        let items = (event.clipboardData || event.originalEvent.clipboardData).items;
        console.log(JSON.stringify(items)); // will give you the mime types
        for (let index in items) {
            let item = items[index];
            if (item.kind === 'file') {
                console.log(item);
                let blob = item.getAsFile();
                // let reader = new FileReader();
                // reader.onloadend = function (event) {
                //     console.log(event.target.result)
                // }; // data url!
                // reader.readAsDataURL(blob);
                processFunc && processFunc(blob, item.type);
            }
        }
    }
}