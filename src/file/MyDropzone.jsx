import React, { useCallback } from "react";
import { useDropzone } from 'react-dropzone';

export default function MyDropzone(props = {
  className: '',
  handleFilesFunc: (acceptedFiles) => {
    acceptedFiles.forEach((file) => {
      const reader = new FileReader()
      reader.onabort = () => console.log('file reading was aborted')
      reader.onerror = () => console.log('file reading has failed')
      reader.onload = () => {
        const binaryStr = reader.result
        console.log(binaryStr)
      }
      reader.readAsArrayBuffer(file)
    })
  }
}) {
  const { handleFilesFunc, className, children } = props;
  const onDrop = useCallback(handleFilesFunc, [])
  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })
  return (
    <div className={className} {...getRootProps()}>
      {
        isDragActive && <p className="drag-notice">Drop the files here ...</p>
      }
      {children}
    </div>
  )
}