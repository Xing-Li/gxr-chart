import { AutoComplete, Button, Input, Modal } from 'antd';
import axios from 'axios';
import _ from 'lodash';
import PropTypes from "prop-types";
import React from 'react';
import Spinner from '../components/Spinner.jsx';
const { showToast } = require("../util/utils");
const { DefaultFileData, DefaultFolderData } = require('../util/localstorage');

export default class Share extends React.Component {
    static propTypes = {
        value: PropTypes.any,
    };
    static defaultProps = {};

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            options: [],
            email: "",
            users: []
        }
    }

    componentDidMount() {
        const { email } = this.state;
        this.getUserByKeyword(
            email,
            [])
    }

    showModal = () => {
        this.setState({
            visible: true,
        });
    };

    handleOk = e => {
        // console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancel = e => {
        // console.log(e);
        this.setState({
            visible: false,
        });
    };

    getUserByKeyword = async (message, exitedEmail) => {
        let res = await axios.post(`/api/user/getUserByKeyword`, { message: message, exitedEmail: exitedEmail });
        if (!res.data.status) {
            // console.log(res.data.content)
            let arr = _.reduce(res.data.content, (arr, value, index) => {
                arr.push({ value: value.username })
                return arr;
            }, [])
            this.setState({ options: arr, users: res.data.content })
        }
    }

    share = async () => {
        const { email, options, users } = this.state;
        const { value } = this.props;
        let fileKey;
        if (value instanceof FileObj) {
            fileKey = value.file;
        } else if (value instanceof DirObj) {
            fileKey = value.dir;
        }
        let user = _.find(users, (user, index, users) => {
            return user.username === email;
        })
        let res = await axios.post(`/api/file/shareFile/chart`, {
            toUser: user,
            fileKey: fileKey,
        });
        if (!res.data.status) {
            showToast(res.data.message);
        } else {
            showToast(res.data.message);
        }
    }

    render() {
        const { options, email } = this.state;
        return <React.Fragment>
            <Spinner type="i" className="icon fas fa-share-alt-square" title="Share"
                onClick={(e, setLoading) => {
                    this.setState({ visible: true })
                    setLoading && setLoading(false)
                }}></Spinner>
            <Modal style={{ width: "70%" }}
                title="Share Chart"
                visible={this.state.visible}
                onOk={this.handleOk}
                onCancel={this.handleCancel}
            >
                <Input.Group compact>
                    <Button>Share By Email:</Button>
                    <AutoComplete
                        style={{ width: '170px' }}
                        placeholder="Email"
                        value={email}
                        onChange={(value, option) => {
                            this.setState({ email: value }, () => {
                                this.getUserByKeyword(
                                    value,
                                    [])
                            })
                        }}
                        onSelect={(value, option) => {
                            this.setState({ email: value })
                        }}
                        options={options}
                    />
                    <Button type="primary" onClick={(e) => {
                        this.share();
                    }}>Share</Button>
                </Input.Group>
            </Modal>
        </React.Fragment>
    }
}

export class FileObj {
    constructor(file) {
        this.file = file;
    }
}
;

export class DirObj {
    constructor(dir) {
        this[DefaultFolderData.type] = dir;
        this.files = {};
    }
}