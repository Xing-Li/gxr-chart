import { Affix } from 'antd';
import axios from 'axios';
import cx from "classnames";
import JSZip from 'jszip';
import _ from 'lodash';
import moment from 'moment';
import PropTypes from "prop-types";
import React from 'react';
import { Menu, Dropdown, Modal } from 'antd';
import Share, { FileObj, DirObj } from './Share';
import Spinner from '../components/Spinner';
import { Controlled } from 'react-codemirror2';
import 'codemirror';
import 'codemirror/mode/javascript/javascript';
import 'codemirror/addon/hint/show-hint';
import 'codemirror/addon/hint/javascript-hint';
import 'codemirror/addon/fold/foldcode'
import 'codemirror/addon/fold/foldgutter'
import 'codemirror/addon/fold/comment-fold'
import 'codemirror/addon/comment/comment'
import 'codemirror/addon/display/placeholder.js'
import editor from '../commonEditor';
const { isFileImage, ImageCache, formatSizeUnits, formatTime } = require('../util/helpers.js');
const { dataURItoBlob, saveLink, showToast } = require("../util/utils");
const { fileSeparator, getMdFile, openDirs, pasteScreenShot } = require("./fileUtils");
const { DefaultData, imageDataFunc } = require('../block/data');
const {
  getFileNamesKey, getItem, removeItem, setItem, DefaultFileData, DefaultFolderData,
  DefaultFileType, getFileNamesJson, SelectType, getFileType
} = require("../util/localstorage");

const sortDirFileByKey = o => _.sortBy(Object.keys(o), [n => {
  return o[n] instanceof DirObj ? ' ' + n.toLowerCase() : n.toLowerCase();
}]).reduce((r, k) => (r[k] = o[k], r), {});
const fileNameReg = /^[0-9a-zA-Z-+_\.\(\) ]*$/;
const folderNameReg = /^[0-9a-zA-Z-+_]*$/;
const rootDir = Object.assign({}, DefaultFolderData, {
  folderKey: "", name: ""
})
export default class MdFiles extends React.Component {
  static propTypes = {
    wrapper: PropTypes.object,
    className: PropTypes.string,
    mdfiles_transfer: PropTypes.shape({
      acceptedFiles: PropTypes.any,
      downloadNum: PropTypes.number,
      setActiveFileKeyDesc: PropTypes.func,
      getActiveFileKeyDesc: PropTypes.func,
      setPrint: PropTypes.func,
      setLoading: PropTypes.func,
    }),
    userId: PropTypes.string
  };

  static defaultProps = { mdfiles_transfer: {} };
  constructor(props) {
    super(props);
    this.state = {
      uploadUri: null,
      className: this.props.className,
      activeFolderKey: rootDir.folderKey,
      activeFileKey: null,
      rename: false,
      newFile: false,
      newFolder: false,
      fileNamesJson: this.props.userId !== '' ? {} : getFileNamesJson(),
      cloud: {},
      selectType: SelectType.dir,
      needSave: false,
      hoverFile: DefaultFolderData,
      visible: false,
      source: ''
    };
    this.ref = React.createRef();
  }

  setPrint = (print) => {
    const { mdfiles_transfer: { setPrint } } = this.props;
    setPrint && setPrint(print);
  }

  setWrapperLoading = (loading, func) => {
    const { mdfiles_transfer: { setLoading } } = this.props;
    setLoading && setLoading(loading, func)
  }

  componentDidUpdate(preProps, preState) {
    const { fileNamesJson, activeFileKey } = this.state;
    const { mdfiles_transfer: { setActiveFileKeyDesc, getActiveFileKeyDesc,
      acceptedFiles, downloadNum } } = this.props;
    if (preProps.userId !== this.props.userId) {
      this.setState({
        fileNamesJson: this.props.userId !== '' ? {} : getFileNamesJson(),
        cloud: {},
      }, () => {
        this.loadData();
      })
    }
    if (setActiveFileKeyDesc && getActiveFileKeyDesc) {
      let file = activeFileKey !== null ? fileNamesJson[activeFileKey] : null;
      if (preState.activeFileKey !== activeFileKey
        || preState.needSave !== this.state.needSave ||
        (file && file.mtimeMs !== getActiveFileKeyDesc()[3])) {
        setActiveFileKeyDesc(
          file ? [
            file.name,
            (file.size !== undefined ? formatSizeUnits(file.size) : '-'),
            this.state.needSave ? "!" : "",
            file.mtimeMs
          ] : []);
      }
    }
    if (preProps.className !== this.props.className) {
      this.setState({ className: this.props.className })
    }
    if (preProps.mdfiles_transfer.acceptedFiles !== acceptedFiles) {
      this.onImport(acceptedFiles);
    }
    if (preProps.mdfiles_transfer.downloadNum !== downloadNum) {
      showToast("Saving ...");
      let obj = {};
      let func = async (file, tmpfileKey, fileNamesJson) => {
        let item = await this.getRealFileContent(file);
        obj[tmpfileKey] = item;
      }
      let promises = [];
      _.each(fileNamesJson, (file, tmpfileKey, fileNamesJson) => {
        promises.push(func(file, tmpfileKey, fileNamesJson));
      })
      Promise.all(promises).then(results => {
        let graphxrZip = new JSZip();
        _.each(obj, (item, tmpfileKey) => {
          graphxrZip.file(tmpfileKey, item)
        })
        let compressionOption = {
          compression: "DEFLATE",
          compressionOptions: {
            level: 9 // force a compression and a compression level for this file
          }
        };
        graphxrZip.generateAsync({ type: "blob", ...compressionOption })
          .then((blob) => {
            saveLink(URL.createObjectURL(blob),
              `notebook.${moment(new Date()).format("YYYY-MM-DD HH-mm-ss")}.zip`);
            showToast("Save Successful!");
          }, (error) => {
            console.error(error)
          });
      });
    }
  }

  loadData() {
    const { fileNamesJson } = this.state;
    let cb = async () => {
      let selectedFileKeyTmp, content;
      Object.keys(fileNamesJson).forEach((tmpFileKey) => {
        if (fileNamesJson[tmpFileKey].selected) {
          selectedFileKeyTmp = tmpFileKey;
        }
      })
      if (undefined === selectedFileKeyTmp) {
        return;
      }
      let save = await editor.saveCurrentFile();
      content = await this.getFileContent(fileNamesJson[selectedFileKeyTmp])
      this.loadNewFile(content, fileNamesJson[selectedFileKeyTmp]);
      this.setState(_.assign({ activeFileKey: selectedFileKeyTmp, selectType: SelectType.file },
        save ? { fileNamesJson } : {}), () => {
          openDirs(this.state.activeFileKey);
        });
    }
    axios.post(`/api/file/load/chart`, { userId: this.props.userId })
      .then((res) => {
        if (!res.data.status) {
          showToast(res.data.message);
          if (res.data.content instanceof Object) {
            _.each(fileNamesJson, (value, key) => {
              if (fileNamesJson[key] && !res.data.content[key] &&
                getItem(getFileNamesKey() + '.' + key) === undefined) {
                console.error(`local file not exist! ${key}`);
                delete fileNamesJson[key];
              }
            })
            _.each(fileNamesJson, (value, key) => {
              if (fileNamesJson[key] && res.data.content[key] &&
                getItem(getFileNamesKey() + '.' + key) === undefined &&
                fileNamesJson[key].mtimeMs !== res.data.content[key].mtimeMs) {
                console.error(`someone remote have modified! ${key}`);
                delete fileNamesJson[key];
              }
            })
            _.merge(fileNamesJson, _.reduce(res.data.content, (result, value, key) => {
              if (!fileNamesJson[key]) {
                result[key] = _.cloneDeep(value);
              }
              return result;
            }, {}));
            _.each(fileNamesJson, (value, key) => {///fix prev bug
              if (getItem(getFileNamesKey() + '.' + key) !== undefined && res.data.content[key] &&
                fileNamesJson[key].mtimeMs === res.data.content[key].mtimeMs) {
                fileNamesJson[key].mtimeMs = (new Date()).getTime()
              }
            })
            setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
            this.setState({
              cloud: res.data.content,
              fileNamesJson: fileNamesJson,
              uploadUri: res.data.uploadUri
            }, cb);
          }
        }
      }).catch(function (error) {
        // handle error
        console.log(error);
      })
  }

  componentDidMount() {
    editor.init({}, this);
    editor.get().isReady
      .then(() => {
        console.log('Editor.js is ready to work!')
        this.loadData();
        pasteScreenShot((blob, type) => {
          let fileName = `screenshot-${moment(new Date()).format("YYYY-MM-DD+hh-mm-ss")}.${type.replace("image/", "")}`;
          let file = Object.assign({}, DefaultFileData, {
            fileKey: `screenshot/${fileName}`,
            name: fileName,
            type: type
          })
          this.setWrapperLoading(true, () => {
            let reader = new FileReader();
            reader.onloadend = (event) => {
              this.uploadMdFile(file, this.setWrapperLoading, event.target.result).then((result) => {
                if (result) {
                }
              });
            };
            reader.readAsDataURL(blob);
          })
        });
        document.querySelector(".folder").querySelectorAll("details").forEach((details) => {
          details.addEventListener("toggle", this.toggleFunc);
        })
      })
      .catch((reason) => {
        console.log(`Editor.js initialization failed because of ${reason}`)
      });
  }

  async getFileContent(file) {
    const { uploadUri, cloud } = this.state;
    if (file.type === DefaultFolderData.type) {
      return null;
    }
    return getMdFile(uploadUri, cloud, file);
  }

  async getRealFileContent(file) {
    if (file.type === DefaultFolderData.type) {
      return null;
    }
    let item = await this.getFileContent(file);
    if (isFileImage(file)) {
      let response = await fetch(item);
      let blob = await response.blob();
      item = blob;
    }
    return item;
  }

  async onExport() {
    const { activeFileKey, fileNamesJson } = this.state;
    if (activeFileKey === null) {
      showToast("please select a file");
      return;
    }
    let save = await editor.saveCurrentFile();
    let file = fileNamesJson[activeFileKey];
    let markdown = await this.getRealFileContent(file);
    if (isFileImage(file)) {
      saveLink(URL.createObjectURL(markdown), file.name);
    } else {
      let dataStr = `data:text/html;charset=utf-8,${encodeURIComponent(markdown)}`;
      let downloadAnchorNode = document.createElement("a");
      downloadAnchorNode.setAttribute("href", dataStr);
      downloadAnchorNode.setAttribute("download", activeFileKey);
      document.body.appendChild(downloadAnchorNode);
      downloadAnchorNode.click();
      downloadAnchorNode.remove();
    }
    save && this.setState({ fileNamesJson })
  }

  async parse(file) {
    // Always return a Promise
    return new Promise((resolve, reject) => {
      let content = "";
      const reader = new FileReader();
      // Wait till complete
      reader.onloadend = function (e) {
        content = e.target.result;
        resolve(content);
      };
      // Make sure to handle error states
      reader.onerror = function (e) {
        reject(e);
      };
      if (isFileImage(file))
        reader.readAsDataURL(file);
      else
        reader.readAsText(file);
    });
  }

  /**
   * 
   * @param {*} folder
   * @param {*} setLoading 
   * @param {*} state 
   */
  async uploadFolder(folder, setLoading, state) {
    let fd = new FormData();
    fd.append("folderKey", folder.folderKey);
    fd.append("folderName", folder.name);
    fd.append("userId", this.props.userId);
    let res = await axios.post(`/api/file/uploadFolder/chart`, fd);
    if (!res.data.status) {
      const { cloud, fileNamesJson } = this.state;
      cloud[folder.folderKey] = _.cloneDeep(res.data.content);
      fileNamesJson[folder.folderKey] = res.data.content;
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      state !== undefined ? Object.assign(state, { cloud: cloud, fileNamesJson: fileNamesJson }) :
        this.setState({ cloud: cloud, fileNamesJson: fileNamesJson });
    }
    res.data.message && showToast(res.data.message);
    setLoading && setLoading(false);
    return !res.data.status;
  }

  /**
   * 
   * @param {*} file upload file data
   * @param {*} setLoading during upload set loading false
   * @param {*} content transfer content need upload 
   * @param {*} state if need setState
   * @return return boolean means success/fail 
   */
  async uploadMdFile(file, setLoading, content, state) {
    let fd = new FormData();
    fd.append("fileKey", file.fileKey);
    fd.append("fileName", file.name);
    content = (content !== undefined && content !== null) ? content : getItem(getFileNamesKey() + '.' + file.fileKey);
    let data = isFileImage(file) ? dataURItoBlob(content) : new Blob([content], { type: file.type });
    fd.append('data', data);
    fd.append('userId', this.props.userId);
    let res = await axios.post(`/api/file/uploadFile/chart`, fd);
    if (!res.data.status) {
      const { cloud, fileNamesJson } = this.state;
      cloud[file.fileKey] = _.cloneDeep(res.data.content);
      fileNamesJson[file.fileKey] = res.data.content;
      removeItem(getFileNamesKey() + '.' + file.fileKey);
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      state ? Object.assign(state, { cloud: cloud, fileNamesJson: fileNamesJson }) :
        this.setState({ cloud: cloud, fileNamesJson: fileNamesJson });
    }
    res.data.message && showToast(res.data.message);
    setLoading && setLoading(false);
    return !res.data.status;
  }

  async onImport(files = []) {
    const { fileNamesJson, activeFileKey, activeFolderKey, selectType, uploadUri, cloud } = this.state;
    /** only for file **/
    let content = undefined,
      fileName = undefined,
      fileKey = undefined;
    /** only for folder **/
    let folderName = undefined,
      folderKey = undefined;
    let explain = async (zip) => {
      const promiseList = _.map(_.keys(zip.files), function (zipFileKey) {
        let file = zip.files[zipFileKey];
        if (file.dir) {
          return [
            zipFileKey,
            null,
            DefaultFolderData.type
          ]
        } else if (/(.jpg|.png|.gif|.ps|.jpeg|.jfif)$/.test(zipFileKey.toLowerCase())) {
          return file.async('base64').then(function (base64) {
            let type = `image/${/(.jpg|.png|.gif|.ps|.jpeg|.jfif)$/.exec(zipFileKey.toLowerCase())[0].substring(1)}`
            return [
              zipFileKey,
              `data:${type};base64,${base64}`,
              type
            ]
          })
        } else {
          return file.async('string').then(function (string) {
            let type = ~zipFileKey.lastIndexOf('.') ? `text/${zipFileKey.substring(zipFileKey.lastIndexOf('.') + 1)}` : DefaultFileType
            return [
              zipFileKey,
              string,
              type
            ]
          })
        }
      })
      await Promise.all(promiseList).then(values => {
        _.forEach(values, value => {
          let type = value[2];
          if (type === DefaultFolderData.type) {
            let tmpFolderKey = value[0];
            if (fileNamesJson[tmpFolderKey]) {
              return;
            }
            let erazerLastChar = tmpFolderKey.substring(0, tmpFolderKey.length - 1);
            folderKey = tmpFolderKey,
              folderName = erazerLastChar.substring(erazerLastChar.lastIndexOf(fileSeparator));
            fileNamesJson[folderKey] = Object.assign({}, DefaultFolderData,
              { folderKey: folderKey, name: folderName, selected: false, type: type });
          } else {
            let tmpFileKey = value[0];
            let tmpContent = value[1];
            if (fileNamesJson[tmpFileKey] !== undefined && !window.confirm(`Overwrite ${tmpFileKey}?`)) {
              return;
            }
            content = tmpContent,
              fileKey = tmpFileKey,
              fileName = tmpFileKey.substring(tmpFileKey.lastIndexOf(fileSeparator) + 1);
            setItem(getFileNamesKey() + '.' + fileKey, content);
            fileNamesJson[fileKey] = Object.assign({}, DefaultFileData,
              { fileKey: fileKey, name: fileName, selected: false, type: type });
          }
        })
      });
    }
    for (let index = 0; index < files.length; index++) {
      let file = files[index];
      if (!file) {
        continue;
      }
      if (file.type === "application/zip") {
        // 1) read the Blob
        await JSZip.loadAsync(file, 'blob') // 1) read the Blob
          .then(explain, function (e) {
            console.error(e);
            showToast(`GraphXR unrecognized file(${file.name})`);
          });
      } else {
        let tmpContent = await this.parse(file);
        let tmpFileName = file.name;
        let tmpFileKey = (!file.path || file.path === file.name) ?
          this.getFileKey(selectType === SelectType.dir ? activeFolderKey : activeFileKey, file.name) :
          (file.path.startsWith(fileSeparator) ? file.path.substring(1) : file.path);
        let tmpType = ~tmpFileName.lastIndexOf('.') ?
          `text/${tmpFileName.substring(tmpFileName.lastIndexOf('.') + 1)}` : DefaultFileType;
        if (fileNamesJson[tmpFileKey] !== undefined && !window.confirm(`Overwrite ${tmpFileKey}?`)) {
          continue;
        }
        content = tmpContent;
        fileKey = tmpFileKey;
        fileName = tmpFileName;
        setItem(getFileNamesKey() + '.' + fileKey, content);
        fileNamesJson[fileKey] = Object.assign({}, DefaultFileData,
          { fileKey: fileKey, name: fileName, selected: false, type: file['type'] || tmpType });
      }
    }
    let promiseList = [];
    this.setWrapperLoading(true, async () => {
      let state = {};
      _.map(fileNamesJson, (file, fileKey) => {
        if (!cloud[fileKey]) {
          if (file.type === DefaultFolderData.type) {
            promiseList.push(this.uploadFolder(file, undefined, state))
          } else {
            promiseList.push(this.uploadMdFile(file, undefined, undefined, state));
          }
        }
      })
      await Promise.all(promiseList).then(async (values) => {
        if (fileKey !== undefined) {
          await editor.saveCurrentFile();
          Object.keys(fileNamesJson).forEach((tmpfileKey) => {
            fileNamesJson[tmpfileKey].selected = false;
          })
          fileNamesJson[fileKey].selected = true;
          setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
          this.loadNewFile(content, fileNamesJson[fileKey]);
          Object.assign(state, { activeFileKey: fileKey, selectType: SelectType.file, fileNamesJson: fileNamesJson });
        }
        this.setState(state);
        this.setWrapperLoading(false);
        document.querySelector(".folder").querySelectorAll("details").forEach((details) => {
          details.removeEventListener("toggle", this.toggleFunc);
          details.addEventListener("toggle", this.toggleFunc);
        })
      });
    })
  }
  /**
   * used to rename or move file
   * @param {*} e  only read the `target.value` that regard as newFileName
   * @param {*} oldFileKey 
   * @param {*} referFileKey transfer this if moved file position 
   */
  renameOrMoveFileFunc = async (e, oldFileKey, referFileKey, setLoading) => {
    const { activeFileKey, fileNamesJson, uploadUri, cloud } = this.state;
    referFileKey = referFileKey === undefined ? oldFileKey : referFileKey;
    const oldFile = fileNamesJson[oldFileKey];
    const newFileName = e.target.value;
    const newFileKey = this.getFileKey(referFileKey, newFileName);
    if (oldFileKey === newFileKey) {
      e.target.value = oldFile.name;
      this.setState({ rename: false }, () => {
        showToast('Can not operate.');
      })
      return false;
    }
    if (!newFileName.match(fileNameReg)) {
      e.target.value = oldFile.name;
      this.setState({ rename: false }, () => {
        showToast('File name should only containg letters and numbers, please rename image file and upload.');
      })
      return false;
    }
    let content;
    if (isFileImage(oldFile)) {
      content = undefined;
    } else if (oldFileKey === activeFileKey) {
      content = await editor.save();
    } else {
      content = getItem(getFileNamesKey() + '.' + oldFileKey);
    }
    if (content === undefined ||
      (cloud[oldFileKey] && cloud[oldFileKey].mtimeMs === oldFile.mtimeMs)) {
      //not in cache or not modify, just rename file
      let res = await axios.post(`/api/file/renameFile/chart`,
        { oldFileKey: oldFileKey, newFileKey: newFileKey, userId: this.props.userId });
      if (res.data.status) {
        res.data.message && showToast(res.data.message);
        setLoading && setLoading(false);
        e.target.value = oldFile.name;
        this.setState({ rename: false })
        return false;
      }
      res.data.message && showToast(res.data.message);
      setLoading && setLoading(false);
      cloud[newFileKey] = _.cloneDeep(res.data.content);
      fileNamesJson[newFileKey] = res.data.content;
      removeItem(getFileNamesKey() + '.' + newFileKey);
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
    } else {
      //upload modified file to path different from old path
      let res = await axios.post(`/api/file/removeFile/chart`,
        { fileKey: oldFileKey, userId: this.props.userId });
      if (res.data.status) {
        res.data.message && showToast(res.data.message);
        setLoading && setLoading(false);
        e.target.value = oldFile.name;
        this.setState({ rename: false })
        return false;
      };
      res = await this.uploadMdFile(
        Object.assign({}, oldFile, { fileKey: newFileKey, name: newFileName }), setLoading, content);
      if (!res) {
        res.data.message && showToast(res.data.message);
        setLoading && setLoading(false);
        e.target.value = oldFile.name;
        this.setState({ rename: false })
        return false;
      }
    }
    if (isFileImage(oldFile)) {
      this.removeFileCache(oldFileKey);
      fileNamesJson[newFileKey].selected = true;
      content = await this.getFileContent(fileNamesJson[newFileKey])
      let fileData = fileNamesJson[newFileKey];
      this.loadNewFile(content, fileData);
      this.setState({
        activeFileKey: newFileKey, selectType: SelectType.file,
        rename: false, fileNamesJson: fileNamesJson, cloud: cloud
      })
    } else if (oldFileKey === activeFileKey) {
      await editor.saveCurrentFile();
      this.removeFileCache(oldFileKey);
      setItem(getFileNamesKey() + '.' + newFileKey, content);
      fileNamesJson[newFileKey].selected = true;
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      let fileData = fileNamesJson[newFileKey];
      this.loadNewFile(content, fileData);
      this.setState({
        activeFileKey: newFileKey, selectType: SelectType.file,
        rename: false, fileNamesJson: fileNamesJson, cloud: cloud
      })
    } else {
      this.removeFileCache(oldFileKey);
      fileNamesJson[newFileKey].selected = false;
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      this.setState({ rename: false, fileNamesJson: fileNamesJson, cloud: cloud })
    }
  };

  switchFile = async (e, fileKey) => {
    const { activeFileKey, selectType, fileNamesJson, uploadUri, cloud } = this.state;
    if (fileKey === activeFileKey) {
      selectType !== SelectType.file && this.setState({ selectType: SelectType.file })
      return;
    }
    let dataLink = e.currentTarget.parentNode.getAttribute("data-link");
    let save = await editor.saveCurrentFile();
    Object.keys(fileNamesJson).forEach((tmpfileKey) => {
      if (fileKey !== tmpfileKey)
        fileNamesJson[tmpfileKey].selected = false;
      else fileNamesJson[tmpfileKey].selected = true;
    })
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
    this.setWrapperLoading(true, async () => {
      let content = await this.getFileContent(fileNamesJson[fileKey])
      this.loadNewFile(content, fileNamesJson[fileKey]);
      let state = _.assign({
        activeFileKey: fileKey, selectType: SelectType.file, activeFolderKey: dataLink
      }, save ? { fileNamesJson } : {})
      this.setState(state, () => {
        this.setWrapperLoading(false)
      });
    })
  }

  loadNewFile = (content, fileData) => {
    if (isFileImage(fileData)) {
      editor.value(JSON.stringify(imageDataFunc(content, fileData.name)));
      editor.setFileData(undefined);
    } else {
      editor.value(content);
      editor.setFileData(fileData);
      editor.autosave();
    }
  }

  getFileKey = (referFileKey, fileName) => {
    return (referFileKey && ~referFileKey.lastIndexOf(fileSeparator)) ? (referFileKey.substring(0, referFileKey.lastIndexOf(fileSeparator)) + fileSeparator + fileName) : fileName;
  }
  getFolderKey = (referFileKey, folderName) => {
    return (referFileKey && ~referFileKey.lastIndexOf(fileSeparator)) ? (referFileKey.substring(0, referFileKey.lastIndexOf(fileSeparator)) + fileSeparator + folderName + fileSeparator) : folderName + fileSeparator;
  }

  toggleFunc = (e) => {
    const { activeFolderKey, selectType } = this.state;
    const dataLink = e.currentTarget.getAttribute("data-link");
    if (activeFolderKey !== dataLink) {
      this.setState({ activeFolderKey: dataLink, selectType: SelectType.dir });
    } else if (selectType !== SelectType.dir) {
      this.setState({ selectType: SelectType.dir })
    }
  }

  newFolderFunc = (e) => {
    const { activeFileKey, activeFolderKey, selectType, fileNamesJson } = this.state;
    const folderName = e.target.value;
    e.target.value = "";
    if (!folderName.match(folderNameReg)) {
      showToast('Folder name should only containg letters and numbers, please rename folder and upload.');
      return;
    }
    if ("" === folderName) {
      showToast("Forder name can not be empty!")
      return;
    }
    let folderKey = this.getFolderKey(selectType === SelectType.dir ? activeFolderKey : activeFileKey, folderName);
    if (fileNamesJson[folderKey]) {
      showToast("Forder name repeated!")
      return;
    }
    fileNamesJson[folderKey] = Object.assign({}, DefaultFolderData, {
      folderKey: folderKey, name: folderName
    });
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
    this.setState({ newFolder: false });
    this.setWrapperLoading(true, () => {
      this.uploadFolder(fileNamesJson[folderKey], this.setWrapperLoading).then((value) => {
        this.bindFolderEvent(folderKey);
      })
    })
  }

  bindFolderEvent = (...folderKeys) => {
    document.querySelector(".local").querySelectorAll("details").forEach((details) => {
      if (~folderKeys.indexOf(details.getAttribute("data-link"))) {
        details.removeEventListener("toggle", this.toggleFunc);
        details.addEventListener("toggle", this.toggleFunc);
      }
    })
  }

  saveFileNamesJson() {
    const { fileNamesJson } = this.state;
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
    this.setState({ fileNamesJson: fileNamesJson });
  }

  newFileFunc = async (e) => {
    const { activeFileKey, activeFolderKey, selectType, fileNamesJson, uploadUri, cloud } = this.state;
    const fileName = e.target.value;
    e.target.value = "";
    if (!fileName.match(fileNameReg)) {
      showToast('File name should only containg letters and numbers, please rename image file and upload.');
      return;
    }
    if ("" === fileName) {
      showToast("File name can not be empty!")
      return;
    }
    let fileKey = this.getFileKey(selectType === SelectType.dir ? activeFolderKey : activeFileKey, fileName);
    if (fileNamesJson[fileKey] !== undefined && !window.confirm(`Overwrite ${fileKey}?`)) {
      return;
    }
    if (activeFileKey) {
      await editor.saveCurrentFile();
      editor.value(DefaultData);
    }
    let content = await editor.save();
    setItem(getFileNamesKey() + '.' + fileKey, content);
    fileNamesJson[fileKey] = Object.assign({}, DefaultFileData, {
      fileKey: fileKey, name: fileName, selected: false, type: DefaultFileType, mtimeMs: new Date().getTime()
    });
    Object.keys(fileNamesJson).forEach((tmpfileKey) => {
      if (fileKey !== tmpfileKey)
        fileNamesJson[tmpfileKey].selected = false;
      else fileNamesJson[tmpfileKey].selected = true;
    })
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
    this.loadNewFile(content, fileNamesJson[fileKey]);
    let state = {};
    this.setWrapperLoading(true, () => {
      this.uploadMdFile(fileNamesJson[fileKey], this.setWrapperLoading, undefined, state).then((value) => {
        this.setState(Object.assign({}, state,
          { activeFileKey: fileKey, selectType: SelectType.file, rename: false, newFile: false }, () => {
          }));
      });
    })
  }

  removeFileFunc = async (e, setLoading, activeFileKeyTmp) => {
    const activeFileKey = activeFileKeyTmp || this.state.activeFileKey;
    if (activeFileKey === null) {
      setLoading && setLoading(false);
      return;
    }
    await editor.saveCurrentFile();
    const { fileNamesJson, cloud } = this.state;
    let res = await axios.post(`/api/file/removeFile/chart`, { fileKey: activeFileKey, userId: this.props.userId });
    if (!res.data.status) {
      this.removeFileCache(activeFileKey);
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      editor.setFileData(undefined);
      editor.value(DefaultData);
      let state = {
        fileNamesJson: fileNamesJson, cloud: cloud,
        selectType: SelectType.dir, rename: false
      }
      activeFileKey === this.state.activeFileKey && _.assign(state, { activeFileKey: null })
      this.setState(state)
    }
    showToast(res.data.message);
    setLoading && setLoading(false);
  }

  removeFolderFunc = async (e, setLoading, activeFolderKeyTmp) => {
    const { activeFileKey, fileNamesJson, cloud } = this.state;
    const activeFolderKey = activeFolderKeyTmp || this.state.activeFolderKey;
    if (activeFolderKey === null) {
      setLoading && setLoading(false);
      return;
    }
    let res = await axios.post(`/api/file/removeFolder/chart`, { folderKey: activeFolderKey, userId: this.props.userId });
    if (!res.data.status) {
      let state = {};
      if (activeFileKey !== null && activeFileKey.startsWith(activeFolderKey)) {
        await editor.saveCurrentFile();
        this.removeFileCache(activeFileKey)
        setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
        editor.setFileData(undefined);
        editor.value(DefaultData);
        _.assign(state, { activeFileKey: null, selectType: SelectType.dir, rename: false })
      }
      let removeFileKeys = _.keys(fileNamesJson).filter(tmpFileKey => { return tmpFileKey.startsWith(activeFolderKey) });
      _.each(removeFileKeys, fileKey => {
        this.removeFileCache(fileKey);
      })
      setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
      activeFolderKey === this.state.activeFolderKey && _.assign(state, { activeFolderKey: rootDir.folderKey })
      this.setState(_.assign(state,
        { fileNamesJson: fileNamesJson, cloud: cloud }));
    }
    showToast(res.data.message);
    setLoading && setLoading(false);
  }

  removeFileCache = (activeFileKey) => {
    const { cloud, fileNamesJson } = this.state;
    removeItem(getFileNamesKey() + '.' + activeFileKey);
    if (isFileImage(fileNamesJson[activeFileKey])) {
      delete ImageCache[activeFileKey];
    }
    delete cloud[activeFileKey];
    delete fileNamesJson[activeFileKey];
  }

  setNeedSave = (needSave) => {
    const { activeFileKey, fileNamesJson } = this.state;
    if (activeFileKey !== null && isFileImage(fileNamesJson[activeFileKey])) {
      needSave = false;
    }
    this.setState({ needSave: needSave });
  }

  render() {
    const { activeFileKey, activeFolderKey, selectType,
      rename, newFile, newFolder, fileNamesJson, className, cloud,
      hoverFile, visible, source } = this.state;
    const menu = (
      <Menu onClick={async (info) => {
        switch (info.key) {
          case "removeFile":
            if (getFileType(hoverFile) === SelectType.dir) {
              this.removeFolderFunc(undefined, undefined, hoverFile.folderKey)
            } else {
              this.removeFileFunc(undefined, undefined, hoverFile.fileKey)
            }
            break;
          case "displayFileSource":
            let content = await this.getFileContent(hoverFile)
            if (isFileImage(hoverFile)) {
              content = JSON.stringify(imageDataFunc(content, hoverFile.name), null, 2);
            } else {
              content = JSON.stringify(JSON.parse(content), null, 2)
            }
            this.setState({ visible: true, source: content })
            break;
          case "copySource":
            navigator.permissions.query({ name: "clipboard-write" }).then(async result => {
              if (result.state === "granted" || result.state === "prompt") {
                let content = await this.getFileContent(hoverFile)
                if (isFileImage(hoverFile)) {
                  content = JSON.stringify(imageDataFunc(content, hoverFile.name), null, 2);
                } else {
                  content = JSON.stringify(JSON.parse(content), null, 2)
                }
                /* write to the clipboard now */
                navigator.clipboard.writeText(content).then(function () {
                  showToast("copy success!")
                }, function () {
                  showToast("copy failed!")
                });
              } else {
                showToast("no right to write clipboard!")
              }
            }).catch((err) => {
              console.error(err);
            });
            break;
          case "revertFile":
            break;
        }
      }}>
        <Menu.Item key="removeFile" >remove {getFileType(hoverFile)} {hoverFile.folderKey || hoverFile.fileKey}</Menu.Item>
        <Menu.Item key="displayFileSource" disabled={getFileType(hoverFile) !== SelectType.file} >
          display {hoverFile.folderKey || hoverFile.fileKey} source</Menu.Item>
        <Menu.Item key="copySource" disabled={getFileType(hoverFile) !== SelectType.file} >
          copy {hoverFile.folderKey || hoverFile.fileKey} source</Menu.Item>
        <Menu.Item key="revertFile" disabled={getFileType(hoverFile) !== SelectType.file} >
          revert {hoverFile.folderKey || hoverFile.fileKey}</Menu.Item>
      </Menu>
    );
    const displayObj = new DirObj(rootDir);
    const rootObj = new DirObj(null);
    rootObj.files["root"] = displayObj;
    _.map(fileNamesJson, (file, fileKey) => {
      if (~fileKey.indexOf(fileSeparator)) {
        let tmpObj = displayObj;
        let dir = tmpObj.dir.folderKey;
        let arr = fileKey.split(fileSeparator).filter(value => value !== '')
        for (let index = 0; index < arr.length - 1; index++) {
          dir += (arr[index] + fileSeparator);
          if (tmpObj.files[arr[index]] === undefined) {
            tmpObj.files[arr[index]] = new DirObj(Object.assign({}, DefaultFolderData, {
              folderKey: dir, name: arr[index]
            }));
          }
          tmpObj = tmpObj.files[arr[index]];
        }
        if (tmpObj.files[arr[arr.length - 1]] === undefined) {
          if (file.type !== DefaultFolderData.type) {
            tmpObj.files[arr[arr.length - 1]] = new FileObj(file);
          } else {
            tmpObj.files[arr[arr.length - 1]] = new DirObj(file);
          }
        }
      } else {
        if (file.type !== DefaultFolderData.type) {
          displayObj.files[fileKey] = new FileObj(file);
        } else {
          displayObj.files[fileKey] = new DirObj(file);
        }
      }
    })
    return (
      <React.Fragment>
        <div className={`folder ${className || ''}`} ref={this.ref}>
          <Affix target={() => document.querySelector("#folder .ant-drawer-body")} offsetTop={0}>
            <div className="header d-flex justify-content-start">
              <div className=" d-flex flex-rows">
                <i className="icon fas fa-file" title={`new file(Ctrl+Alt+N)`}
                  onClick={() => {
                    this.setState({ rename: false, newFile: true, newFolder: false }, () => {
                      document.querySelector(".new-file.active input").focus();
                    })
                  }}></i>
                <i className="icon fas fa-folder-plus" title={`new folder`}
                  onClick={(e) => {
                    this.setState({ rename: false, newFile: false, newFolder: true }, () => {
                      document.querySelector(".new-folder.active input").focus();
                    })
                  }}></i>
                <Spinner type="i" className="icon fas fa-trash"
                  title={`remove selected file(Ctrl+Alt+D)`} onClick={(e, setLoading) => {
                    if (activeFileKey && selectType === SelectType.file &&
                      window.confirm(`Are you sure remove ${activeFileKey} ?`)) {
                      this.removeFileFunc(e, setLoading);
                    } else if (selectType === SelectType.dir &&
                      window.confirm(`Are you sure remove ${activeFolderKey}:\n${_.keys(fileNamesJson).filter(tmpFileKey => {
                        return tmpFileKey.startsWith(activeFolderKey)
                      }).join('\n')
                        } ?`)) {
                      this.removeFolderFunc(e, setLoading);
                    } else {
                      setLoading && setLoading(false);
                    }
                  }}></Spinner>
                <i className="icon fas fa-pen" title={`rename selected file(F2)`} onClick={() => {
                  activeFileKey && (this.setState({ rename: true, newFile: false }, () => {
                    document.querySelector(".file-name.active.rename input").focus();
                  }))
                }}></i>
                <i className="icon fas fa-file-export" title="export current file(Ctrl+Alt+E)"
                  onClick={() => this.onExport()} ></i>
              </div>
            </div>
          </Affix>
          <div className={cx(`new-file`,
            { "active": newFile }
          )}> &nbsp;<input type="text" className="form-control"
            onKeyDown={(e) => {
              if (e.key === 'Escape') {
                this.setState({ newFile: false })
              } else if (e.keyCode === '\n'.charCodeAt() || e.keyCode === '\r'.charCodeAt()) {
                this.newFileFunc(e);
              }
            }}
            onBlur={(e) => {
              e.target.value = '';
              this.setState({ newFile: false })
            }}
          ></input></div>
          <div className={cx(`new-folder`,
            { "active": newFolder }
          )}> &nbsp;<input type="text" className="form-control"
            onKeyDown={(e) => {
              if (e.key === 'Escape') {
                e.target.value = '';
                this.setState({ newFolder: false })
              } else if (e.keyCode === '\n'.charCodeAt() || e.keyCode === '\r'.charCodeAt()) {
                this.newFolderFunc(e);
              }
            }}
            onBlur={(e) => {
              e.target.value = '';
              this.setState({ newFolder: false })
            }}
          ></input></div>
          <Dropdown overlay={menu} trigger={['contextMenu']}>
            <div>
              <this.RecursionLocal local={rootObj}></this.RecursionLocal>
            </div>
          </Dropdown>
        </div>
        <Modal
          title="Source"
          visible={visible}
          onOk={() => {
            this.setState({
              visible: false,
            });
          }}
          onCancel={() => {
            this.setState({
              visible: false,
            });
          }}
        >
          {visible && <Controlled className={`code-edit-cm`}
            editorDidMount={(cm, value, cb) => {
            }}
            onFocus={(cm) => {
            }}
            value={source}
            options={_.assign({
              mode: 'javascript',
              theme: 'base16-light',
              lineNumbers: true,
              lineWrapping: false,
              viewportMargin: Infinity,
              styleSelectedText: true,
              matchBrackets: false,
              styleActiveLine: false,
              nonEmpty: false,
              foldGutter: true,
              placeholder: "",
              gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
              readOnly: true,
              extraKeys: {
                "F10": function (cm) {
                  cm.setOption("fullScreen", !cm.getOption("fullScreen"));
                },
                "Esc": (cm) => {
                  if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
                },
              }
            })}
          />}
        </Modal>
      </React.Fragment>
    );
  }

  onDropFunc = (e) => {
    const { fileNamesJson } = this.state;
    e.preventDefault();
    let dragFileKey = e.dataTransfer.getData("dragFileKey");
    let dragFile = fileNamesJson[dragFileKey];
    if (!dragFile) {
      return;
    }
    let referFileKey = e.currentTarget.getAttribute("data-link");
    this.renameOrMoveFileFunc({ target: { value: dragFile.name } }, dragFileKey, referFileKey);
  }
  RecursionLocal = (props = { local: {} }) => {
    const { local } = props;
    const { activeFileKey, activeFolderKey, selectType,
      rename, uploadUri, cloud, fileNamesJson } = this.state;
    return _.map(sortDirFileByKey(local.files), (value, key, kv) => {
      if (value instanceof DirObj) {
        const needUploadArr = _.filter(value.files, (value) => {
          if (value instanceof DirObj) {
            return false;
          } else if (value instanceof FileObj) {
            const file = value.file;
            const fileName = file.name;
            const fileKey = file.fileKey;
            const ver = {
              "untracked": !cloud[fileKey],
              "modified": !!cloud[fileKey] && cloud[fileKey].mtimeMs !== file.mtimeMs
            };
            return ver.untracked || ver.modified;
          } else {
            return false;
          }
        });
        const fileName = key;
        return <details key={value.dir.folderKey}
          data-link={value.dir.folderKey}
          className={cx(
            { "local": value.dir.folderKey === rootDir.folderKey },
            { "active": activeFolderKey === value.dir.folderKey },
            { "deep": activeFolderKey === value.dir.folderKey && selectType === SelectType.dir })}>
          <summary data-link={value.dir.folderKey}
            onMouseOver={() => { this.setState({ hoverFile: value.dir }) }}
            onDrop={this.onDropFunc}
            onDragOver={(e) => {
              e.preventDefault();
            }}>{fileName}
            <div className="folder-name">{value.dir.folderKey || "root"}</div>
          </summary>
          <div className={`icons`}>
            <Spinner type="i"
              className={cx(`icon fas fa-cloud-upload-alt`,
                { "hide": (needUploadArr.length === 0) })}
              title={`Upload ${needUploadArr.length} files`}
              onClick={(e, setLoading) => {
                const promiseList = [];
                _.each(needUploadArr, value => {
                  promiseList.push(this.uploadMdFile(value.file));
                })
                if (promiseList.length === 0) {
                  setLoading && setLoading(false);
                  return;
                }
                Promise.all(promiseList).then(values => {
                  setLoading && setLoading(false)
                });
              }}></Spinner>
            <Share value={value}></Share>
          </div>
          {_.keys(value.files).length > 0 && <this.RecursionLocal local={value}></this.RecursionLocal>}
        </details>
      } else if (value instanceof FileObj) {
        const file = value.file;
        const fileName = file.name;
        const fileKey = file.fileKey;
        const ver = {
          "untracked": !cloud[fileKey],
          "modified": !!cloud[fileKey] && cloud[fileKey].mtimeMs !== file.mtimeMs
        };
        return <div key={fileKey}
          data-link={fileKey}
          className={cx("file", { "active": activeFileKey === fileKey })}
          draggable="true"
          onMouseOver={() => { this.setState({ hoverFile: file }) }}
          onDrop={this.onDropFunc}
          onDragOver={(e) => {
            e.preventDefault();
          }}
          onDragStart={(e) => {
            e.dataTransfer.setData("dragFileKey", e.currentTarget.getAttribute("data-link"))
          }}
          onClick={(e) => {
            if (e.target.tagName === 'I') return;
            this.switchFile(e, fileKey)
          }}>
          <div className={cx(`file-name`,
            { "active": activeFileKey === fileKey },
            { "deep": activeFileKey === fileKey && selectType === SelectType.file },
            { "rename": activeFileKey === fileKey && rename },
            ver
          )}>
            <div className="file-title">
              <div>{fileKey}</div>
              <div>{formatTime(file.mtimeMs)}</div>
              {ver.untracked && <div>~untracked</div>}
              {ver.modified && <div>~modified</div>}
              {file.size !== undefined && <div>{formatSizeUnits(file.size)}</div>}
            </div>
            <span>{fileName}</span>
            <input className="form-control file-name-rename" type="text" defaultValue={fileName}
              onKeyDown={(e) => {
                if (e.key === "Escape") {
                  e.target.value = fileName;
                  this.setState({ rename: false })
                } else if (e.keyCode === '\n'.charCodeAt() || e.keyCode === '\r'.charCodeAt()) {
                  this.renameOrMoveFileFunc(e, fileKey);
                }
              }}
              onBlur={(e) => { e.target.value = fileName; this.setState({ rename: false }) }}
            ></input>
          </div>
          <div className={`icons`}>
            <Spinner type="i"
              className={cx(`icon fas fa-cloud-upload-alt`,
                { "hide": !(ver.untracked || ver.modified) })}
              title="Upload to server"
              onClick={(e, setLoading) => {
                this.uploadMdFile(file, setLoading)
              }}></Spinner>
            <Share value={value}></Share>
          </div>
        </div>
      } else {
        console.error("error value!");
        return;
      }
    })
  }
}