import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import editor from "./commonEditor";
import Util from './util/util';
const { getNodesByLabel } = require('./common/graphOps.js');

function handleCopyOpenerGlobalVariable() {
    if (window.opener && window.opener._app) {
        Object.keys(window.opener)
            .filter((k) => /^_[a-z]+$/gi.test(k))
            .forEach((k) => {
                window[k] = window.opener[k];
            });
        window._app = window.opener._app;
        window._drawing = window._app.controller.drawing;
        window._graph = window._app.controller.graph

        window.API = window.opener.API;
        window.GXR = window.opener._GXR;
        window.AppActions = window.opener._AppActions
        window.THREE = window.opener.THREE;

        window.Util = Util;

        window._ = window.opener._
        window.HistoryLine = window._app.controller ? window._app.controller.graphController.historyLine : null;

        window.editor = editor;
        window.resizeTo(1440, 920);
    }
}
handleCopyOpenerGlobalVariable();
getNodesByLabel()
const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);