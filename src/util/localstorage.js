import _ from 'lodash';
let storage = window.localStorage;
export const FILE_NAMES = ".fileNames";

export const SelectType = {
  dir: "dir",
  file: 'file'
}
export const DefaultFileType = "text/plain";
export const DefaultFileData = {
  fileKey: "fileKey", name: "fileName", path: "event.target.value", selected: false, type: DefaultFileType,
  size: 0, mtimeMs: 0
}
export const DefaultFolderData = {
  folderKey: "folderKey", name: "folderName", selected: false, type: SelectType.dir
}

export const getFileType = function (file) {
  return file.type === SelectType.dir ? SelectType.dir : SelectType.file;
}


export function getFileNamesJson() {
  let fileNamesData = getItem(getFileNamesKey());
  let fileNamesJson = fileNamesData ? JSON.parse(fileNamesData) : {};
  let change = false;
  _.each(_.values(fileNamesJson), (file) => {
    file.name && (file.type !== DefaultFolderData.type) && !file.fileKey && (file.fileKey = file.name) && (change = true);
  })
  if (change) {
    setItem(getFileNamesKey(), JSON.stringify(fileNamesJson));
  }
  return fileNamesJson;
}

export function getFileNamesKey() {
  return getKey() + FILE_NAMES;
}

export function getKey() {
  return `graphxr.chart.${window.opener.globalVariable.project._id}`;
}

export function getItem(key) {
  try {
    const serializedVal = storage.getItem(key)
    if (serializedVal === null) return undefined
    return serializedVal
  } catch (e) {
    return undefined
  }
}
/**
 * save to localstorage
 * @param {*} key 
 * @param {*} val 
 */
export function setItem(key, val) {
  try {
    const serializedVal = val
    storage.setItem(key, serializedVal)
    return true
  } catch (e) {
    return false
  }
}

export function removeItem(key) {
  try {
    storage.removeItem(key)
    return true
  } catch (e) {
    return false
  }
}