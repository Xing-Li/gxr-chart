import _ from 'lodash'

const Util = function() {};

Util.loadJS = function (jsURL) {
    return new Promise((resolve, reject) => {

        if (_.find(document.getElementsByTagName('script'), e => e.src === jsURL)) {
            resolve("done");
        } else {

            let jsDom = document.createElement('script');
            jsDom.type = 'text/javascript';
            jsDom.async = true;
            jsDom.src = jsURL;
            jsDom.onload = function () {
                resolve('done');
            };
            let s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(jsDom, s);
        }

    })

}


Util.sleep = m => new Promise(r => setTimeout(r, m))

export default Util