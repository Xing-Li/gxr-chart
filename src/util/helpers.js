var _ = require("lodash")
/**
 * Helpers
 */
const escapeTest = /[&<>"']/;
const escapeReplace = /[&<>"']/g;
const escapeTestNoEncode = /[<>"']|&(?!#?\w+;)/;
const escapeReplaceNoEncode = /[<>"']|&(?!#?\w+;)/g;
const escapeReplacements = {
  '&': '&amp;',
  '<': '&lt;',
  '>': '&gt;',
  '"': '&quot;',
  "'": '&#39;'
};
const unescapeReplacements = _.invert(escapeReplacements)
export function unescapeHtml(html) {
  if (!html) {
    return;
  }
  return html.replace(new RegExp(`${Object.values(escapeReplacements).join("|")}`, "g"), getUnescapeReplacement);
}
const getEscapeReplacement = (ch) => escapeReplacements[ch];
const getUnescapeReplacement = (ch) => unescapeReplacements[ch];
export function escape(html, encode) {
  if (encode) {
    if (escapeTest.test(html)) {
      return html.replace(escapeReplace, getEscapeReplacement);
    }
  } else {
    if (escapeTestNoEncode.test(html)) {
      return html.replace(escapeReplaceNoEncode, getEscapeReplacement);
    }
  }

  return html;
}

const unescapeTest = /&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/ig;

export function unescape(html) {
  // explicitly match decimal, hex, and named HTML entities
  return html.replace(unescapeTest, (_, n) => {
    n = n.toLowerCase();
    if (n === 'colon') return ':';
    if (n.charAt(0) === '#') {
      return n.charAt(1) === 'x'
        ? String.fromCharCode(parseInt(n.substring(2), 16))
        : String.fromCharCode(+n.substring(1));
    }
    return '';
  });
}

const caret = /(^|[^\[])\^/g;
export function edit(regex, opt) {
  regex = regex.source || regex;
  opt = opt || '';
  const obj = {
    replace: (name, val) => {
      val = val.source || val;
      val = val.replace(caret, '$1');
      regex = regex.replace(name, val);
      return obj;
    },
    getRegex: () => {
      return new RegExp(regex, opt);
    }
  };
  return obj;
}

const nonWordAndColonTest = /[^\w:]/g;
const originIndependentUrl = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i;
export function cleanUrl(sanitize, base, href) {
  if (sanitize) {
    let prot;
    try {
      prot = decodeURIComponent(unescape(href))
        .replace(nonWordAndColonTest, '')
        .toLowerCase();
    } catch (e) {
      return null;
    }
    if (prot.indexOf('javascript:') === 0 || prot.indexOf('vbscript:') === 0 || prot.indexOf('data:') === 0) {
      return null;
    }
  }
  if (base && !originIndependentUrl.test(href)) {
    href = resolveUrl(base, href);
  }
  try {
    href = encodeURI(href).replace(/%25/g, '%');
  } catch (e) {
    return null;
  }
  return href;
}

const baseUrls = {};
const justDomain = /^[^:]+:\/*[^/]*$/;
const protocol = /^([^:]+:)[\s\S]*$/;
const domain = /^([^:]+:\/*[^/]*)[\s\S]*$/;

export function resolveUrl(base, href) {
  if (!baseUrls[' ' + base]) {
    // we can ignore everything in base after the last slash of its path component,
    // but we might need to add _that_
    // https://tools.ietf.org/html/rfc3986#section-3
    if (justDomain.test(base)) {
      baseUrls[' ' + base] = base + '/';
    } else {
      baseUrls[' ' + base] = rtrim(base, '/', true);
    }
  }
  base = baseUrls[' ' + base];
  const relativeBase = base.indexOf(':') === -1;

  if (href.substring(0, 2) === '//') {
    if (relativeBase) {
      return href;
    }
    return base.replace(protocol, '$1') + href;
  } else if (href.charAt(0) === '/') {
    if (relativeBase) {
      return href;
    }
    return base.replace(domain, '$1') + href;
  } else {
    return base + href;
  }
}

export const noopTest = { exec: function noopTest() { } };

export function merge(obj) {
  let i = 1,
    target,
    key;

  for (; i < arguments.length; i++) {
    target = arguments[i];
    for (key in target) {
      if (Object.prototype.hasOwnProperty.call(target, key)) {
        obj[key] = target[key];
      }
    }
  }

  return obj;
}

export function splitCells(tableRow, count) {
  // ensure that every cell-delimiting pipe has a space
  // before it to distinguish it from an escaped pipe
  const row = tableRow.replace(/\|/g, (match, offset, str) => {
    let escaped = false,
      curr = offset;
    while (--curr >= 0 && str[curr] === '\\') escaped = !escaped;
    if (escaped) {
      // odd number of slashes means | is escaped
      // so we leave it alone
      return '|';
    } else {
      // add space before unescaped |
      return ' |';
    }
  }),
    cells = row.split(/ \|/);
  let i = 0;

  if (cells.length > count) {
    cells.splice(count);
  } else {
    while (cells.length < count) cells.push('');
  }

  for (; i < cells.length; i++) {
    // leading or trailing whitespace is ignored per the gfm spec
    cells[i] = cells[i].trim().replace(/\\\|/g, '|');
  }
  return cells;
}

// Remove trailing 'c's. Equivalent to str.replace(/c*$/, '').
// /c*$/ is vulnerable to REDOS.
// invert: Remove suffix of non-c chars instead. Default falsey.
export function rtrim(str, c, invert) {
  const l = str.length;
  if (l === 0) {
    return '';
  }

  // Length of suffix matching the invert condition.
  let suffLen = 0;

  // Step left until we fail to match the invert condition.
  while (suffLen < l) {
    const currChar = str.charAt(l - suffLen - 1);
    if (currChar === c && !invert) {
      suffLen++;
    } else if (currChar !== c && invert) {
      suffLen++;
    } else {
      break;
    }
  }

  return str.substr(0, l - suffLen);
}

export function findClosingBracket(str, b) {
  if (str.indexOf(b[1]) === -1) {
    return -1;
  }
  const l = str.length;
  let level = 0,
    i = 0;
  for (; i < l; i++) {
    if (str[i] === '\\') {
      i++;
    } else if (str[i] === b[0]) {
      level++;
    } else if (str[i] === b[1]) {
      level--;
      if (level < 0) {
        return i;
      }
    }
  }
  return -1;
}

export function checkSanitizeDeprecation(opt) {
  if (opt && opt.sanitize && !opt.silent) {
    console.warn('marked(): sanitize and sanitizer parameters are deprecated since version 0.7.0, should not be used and will be removed in the future. Read more here: https://marked.js.org/#/USING_ADVANCED.md#options');
  }
}

export function isJs(token) {
  if (token && token.type === "code") {
    const lang = (token.lang || '').match(/[a-zA-Z]*/)[0];
    if (~["js", "javascript"].indexOf(String(lang).toLowerCase()))
      return true;
  }
  return false;
}

export function isAutoJs(token) {
  if (isJs(token)) {
    const matches = (token.lang || '').match(/[a-zA-Z]*/g).map(function (value) {
      return value.toLowerCase();
    }).slice(1);
    if (~matches.indexOf("auto"))
      return true;
  }
  return false;
}

export function modal(active = true,
  title = "Confrim something",
  body = "Are you sure ?",
  closeFunc = () => { },
  confirmFunc = () => { }) {
  return new Promise((resolve, reject) => {
    let modalEle = document.querySelector(".modal.fade");
    if (!modalEle) {
      reject(new Error("is visible now ,repeat modal!"));
      return;
    }
    let inactiveFunc = () => {
      modalEle.className = modalEle.className.replace(/\s*active*/g, "");
      modalEle.className = modalEle.className.replace(/\s*show*/g, "");
      document.body.className = document.body.className.replace(/\s*modal-open*/g, "");
      modalEle.removeAttribute("role");
      modalEle.removeAttribute("aria-modal");
      modalEle.setAttribute("aria-hidden", "true");
    }
    if (active) {
      if (!/active/.test(modalEle.className))
        modalEle.className += " active";
      if (!/show/.test(modalEle.className))
        modalEle.className += " show";
      if (!/modal-open/.test(document.body.className))
        document.body.className += " modal-open";
      title !== undefined && (modalEle.querySelector(".modal-title").innerHTML = title);
      if (body !== undefined) {
        typeof body === 'string' && (modalEle.querySelector(".modal-body").innerHTML = body);
        typeof body === 'function' && body(modalEle.querySelector(".modal-body"));
      }
      modalEle.setAttribute("role", "dialog");
      modalEle.setAttribute("aria-modal", "true");
      modalEle.removeAttribute("aria-hidden");
      let close = function (e) {
        e.preventDefault();
        let ret = closeFunc && closeFunc();
        inactiveFunc();
        e.currentTarget.removeEventListener("click", close);
        resolve(ret || false);
      };
      let confirm = function (e) {
        e.preventDefault();
        let ret = confirmFunc && confirmFunc(modalEle.querySelector(".modal-body"));
        inactiveFunc();
        e.currentTarget.removeEventListener("click", confirm);
        resolve(ret || true);
      };
      modalEle.querySelector(".cancel").addEventListener("click", close)
      modalEle.querySelector(".close").addEventListener("click", close)
      modalEle.querySelector(".confirm").addEventListener("click", confirm)
    } else {
      inactiveFunc();
    }
  })
}

export const showWrapperObject = (ret, data = document.body, delare) => {
  if (ret) {
    if (typeof ret === "object") {
      if (!delare) {
        data.innerHTML = `<details>
<summary>${ret.constructor.name}</summary>
</details>`;
        showObject(ret, data.querySelector(":scope > details"));
      } else {
        let key = _.keys(ret)[0];
        let value = ret[key];
        if (value && (typeof value === "object")) {
          data.innerHTML = `<details>
  <summary>${key} : ${value.constructor.name}</summary>
  </details>`;
          showObject(value, data.querySelector(":scope > details"));
        } else {
          data.innerHTML = ``;
          const keyValueTemplate = document.getElementById('keyValue');
          const element = document.importNode(keyValueTemplate.content, true);
          element.querySelector('.key').textContent = key;
          element.querySelector('.value').textContent = value;
          data.appendChild(element);
        }
      }
    } else {
      data.innerHTML = `&nbsp;${ret}`;
    }
  } else {
    data.innerHTML = `&nbsp;${ret + ""}`;
  }
}

export const showObject = (object, parent = document.body) => {
  const keyValueTemplate = document.getElementById('keyValue');
  Object.entries(object).forEach(([key, value]) => {
    if (value && typeof value === "object") {
      // Since this structure is really simple, just create the elements.
      const element = document.createElement('details');
      const summary = element.appendChild(document.createElement('summary'));
      summary.textContent = key;

      element.addEventListener('toggle', () => {
        showObject(value, element)
      }, { once: true });

      parent.appendChild(element);
    } else {
      // Use a template since the structure is somewhat complex.
      const element = document.importNode(keyValueTemplate.content, true);
      element.querySelector('.key').textContent = key;
      element.querySelector('.value').textContent = value;
      parent.appendChild(element);
    }
  });
};

export function showToast(message) {
  let x = document.getElementById("toast")
  let messageContainer = document.getElementById("desc")
  messageContainer.innerHTML = message
  x.className = "show";
  setTimeout(function () { x.className = x.className.replace("show", ""); }, 500);
}

/**
 * @param {String} HTML representing a single element
 * @return {Element}
 */
export function htmlToElement(html) {
  var template = document.createElement('template');
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
}

/**
* @param {String} HTML representing any number of sibling elements
* @return {NodeList} 
*/
export function htmlToElements(html) {
  var template = document.createElement('template');
  template.innerHTML = html;
  return template.content.childNodes;
}

export function isHtml(token) {
  return token.type === 'html';
}

export function isDetails(token) {
  return token.type === 'html' && ~['DETAILS'].indexOf(token.tagName);
}

export function isDetailsEle(ele) {
  return ele && ele.tagName && ~['DETAILS'].indexOf(ele.tagName);//, 'SUMMARY'
}

export function isFileImage(file) {
  const acceptedImageTypes = ['image/jpg', 'image/png', 'image/gif', 'image/ps', 'image/jpeg', 'image/jfif'];
  return file && acceptedImageTypes.includes(file['type'])
}

export function formatSizeUnits(bytes) {
  if (bytes >= 1073741824) { bytes = (bytes / 1073741824).toFixed(2) + "GB"; }
  else if (bytes >= 1048576) { bytes = (bytes / 1048576).toFixed(2) + "MB"; }
  else if (bytes >= 1024) { bytes = (bytes / 1024).toFixed(2) + "KB"; }
  else if (bytes > 1) { bytes = bytes + "bytes"; }
  else if (bytes == 1) { bytes = bytes + "byte"; }
  else { bytes = "0bytes"; }
  return bytes;
}

export const ImageCache = {};

export const sortObjectByKey = o => _.sortBy(Object.keys(o), n => n.toLowerCase()).reduce((r, k) => (r[k] = o[k], r), {});

export const commonHref = href => /^(ftp|http|https):\/\/[^ "]*$/.test(href);

Date.prototype.format = function (fmt) {
  let o = {
    "Y+": this.getFullYear(),
    "M+": this.getMonth() + 1,
    "d+": this.getDate(),
    "h+": this.getHours(),
    "m+": this.getMinutes(),
    "s+": this.getSeconds(),
    "q+": Math.floor((this.getMonth() + 3) / 3), //quarter (of a year) ;
    "S": this.getMilliseconds()
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (let k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
};

export const formatTime = function (timeMs) {
  var date = new Date();
  date.setTime(timeMs)
  return date.format("yyyy-MM-dd hh:mm:ss");
}

export const codeTime = function (nowTimeMs, timeMs) {
  let sub = nowTimeMs - timeMs;
  sub /= 1000;
  if (sub < 60) {
    return `${(sub).toFixed(1)}s ago`;
  }
  sub /= 60;
  if (sub < 60) {
    return `${sub.toFixed(1)}minutes ago`;
  }
  sub /= 60;
  if (sub < 60) {
    return `${sub.toFixed(1)}hours ago`;
  }
  sub /= 24;
  if (sub < 30) {
    return `${sub.toFixed(1)}days ago`;
  }
  sub /= 30;
  if (sub < 12) {
    return `${sub.toFixed(1)}months ago`;
  }
  sub /= 12;
  return `${sub.toFixed(1)}years ago`;
}

export const AsyncFunction = eval(`Object.getPrototypeOf(async function () { }).constructor`)