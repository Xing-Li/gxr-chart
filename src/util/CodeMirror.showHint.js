import CodeMirror from 'codemirror'

CodeMirror.showHint = function (cm, getHints, options) {
    // We want a single cursor position.
    if (cm.somethingSelected()) return;
    if (getHints == null) getHints = cm.getHelper(cm.getCursor(), "hint");
    if (getHints == null) return;

    if (cm.state.completionActive) cm.state.completionActive.close();

    let completion = cm.state.completionActive = new Completion(cm, getHints, options || {});
    CodeMirror.signal(cm, "startCompletion", cm);
    if (completion.options.async)
        getHints(cm, function (hints) { completion.showHints(hints); }, completion.options);
    else
        return completion.showHints(getHints(cm, completion.options));
};

function Completion(cm, getHints, options) {
    this.cm = cm;
    this.getHints = getHints;
    this.options = options;
    this.widget = this.onClose = null;
}

Completion.prototype = {
    close: function () {
        if (!this.active()) return;

        if (this.widget) this.widget.close();
        if (this.onClose) this.onClose();
        this.cm.state.completionActive = null;
        CodeMirror.signal(this.cm, "endCompletion", this.cm);
    },

    active: function () {
        return this.cm.state.completionActive == this;
    },

    pick: function (data, i) {
        let completion = data.list[i];
        if (completion.hint) completion.hint(this.cm, data, completion);
        else this.cm.replaceRange(getText(completion), data.from, data.to);
        this.close();
    },

    showHints: function (data) {
        if (!data || !data.list.length || !this.active()) return this.close();
        this.showWidget(data);
    },

    showWidget: function (data) {
        this.widget = new Widget(this, data);
        CodeMirror.signal(data, "shown");

        let debounce = null, completion = this, finished;
        let closeOn = this.options.closeCharacters || /[\s()\[\]{};:>,]/;
        let startPos = this.cm.getCursor(), startLen = this.cm.getLine(startPos.line).length;

        function done() {
            if (finished) return;
            finished = true;
            completion.close();
            completion.cm.off("cursorActivity", activity);
            CodeMirror.signal(data, "close");
        }
        function isDone() {
            if (finished) return true;
            if (!completion.widget) { done(); return true; }
        }

        function update() {
            if (isDone()) return;
            if (completion.options.async)
                completion.getHints(completion.cm, finishUpdate, completion.options);
            else
                finishUpdate(completion.getHints(completion.cm, completion.options));
        }
        function finishUpdate(data) {
            if (isDone()) return;
            if (!data || !data.list.length) return done();
            completion.widget.close();
            completion.widget = new Widget(completion, data);
        }

        function activity() {
            clearTimeout(debounce);
            let pos = completion.cm.getCursor(), line = completion.cm.getLine(pos.line);
            if (pos.line != startPos.line || line.length - pos.ch != startLen - startPos.ch ||
                pos.ch < startPos.ch || completion.cm.somethingSelected() ||
                (pos.ch && closeOn.test(line.charAt(pos.ch - 1))))
                completion.close();
            else
                debounce = setTimeout(update, 170);
        }
        this.cm.on("cursorActivity", activity);
        this.onClose = done;
    }
};

function getText(completion) {
    if (typeof completion == "string") return completion;
    else return completion.text;
}

function buildKeyMap(options, handle) {
    let baseMap = {
        Up: function () { handle.moveFocus(-1); },
        Down: function () { handle.moveFocus(1); },
        PageUp: function () { handle.moveFocus(-handle.menuSize()); },
        PageDown: function () { handle.moveFocus(handle.menuSize()); },
        Home: function () { handle.setFocus(0); },
        End: function () { handle.setFocus(handle.length); },
        Enter: handle.pick,
        Tab: handle.pick,
        Esc: handle.close
    };
    let ourMap = options.customKeys ? {} : baseMap;
    function addBinding(key, val) {
        let bound;
        if (typeof val != "string")
            bound = function (cm) { return val(cm, handle); };
        // This mechanism is deprecated
        else if (baseMap.hasOwnProperty(val))
            bound = baseMap[val];
        else
            bound = val;
        ourMap[key] = bound;
    }
    if (options.customKeys)
        for (let key in options.customKeys) if (options.customKeys.hasOwnProperty(key))
            addBinding(key, options.customKeys[key]);
    if (options.extraKeys)
        for (let key in options.extraKeys) if (options.extraKeys.hasOwnProperty(key))
            addBinding(key, options.extraKeys[key]);
    return ourMap;
}

function Widget(completion, data) {
    this.completion = completion;
    this.data = data;
    let widget = this, cm = completion.cm, options = completion.options;

    let hints = this.hints = document.createElement("ul");
    hints.className = "CodeMirror-hints";
    this.selectedHint = 0;

    let completions = data.list;
    for (let i = 0; i < completions.length; ++i) {
        let elt = hints.appendChild(document.createElement("li")), cur = completions[i];
        let className = "CodeMirror-hint" + (i ? "" : " CodeMirror-hint-active");
        if (cur.className != null) className = cur.className + " " + className;
        elt.className = className;
        if (cur.render) cur.render(elt, data, cur);
        else elt.appendChild(document.createTextNode(cur.displayText || getText(cur)));
        elt.hintId = i;
    }

    let pos = cm.cursorCoords(options.alignWithWord !== false ? data.from : null);
    let left = pos.left, top = pos.bottom, below = true;
    hints.style.left = left + "px";
    hints.style.top = top + "px";
    // If we're at the edge of the screen, then we want the menu to appear on the left of the cursor.
    let winW = window.innerWidth || Math.max(document.body.offsetWidth, document.documentElement.offsetWidth);
    let winH = window.innerHeight || Math.max(document.body.offsetHeight, document.documentElement.offsetHeight);
    let box = hints.getBoundingClientRect();
    let overlapX = box.right - winW, overlapY = box.bottom - winH;
    if (overlapX > 0) {
        if (box.right - box.left > winW) {
            hints.style.width = (winW - 5) + "px";
            overlapX -= (box.right - box.left) - winW;
        }
        hints.style.left = (left = pos.left - overlapX) + "px";
    }
    if (overlapY > 0) {
        let height = box.bottom - box.top;
        if (box.top - (pos.bottom - pos.top) - height > 0) {
            overlapY = height + (pos.bottom - pos.top);
            below = false;
        } else if (height > winH) {
            hints.style.height = (winH - 5) + "px";
            overlapY -= height - winH;
        }
        hints.style.top = (top = pos.bottom - overlapY) + "px";
    }
    (options.container || document.body).appendChild(hints);

    cm.addKeyMap(this.keyMap = buildKeyMap(options, {
        moveFocus: function (n) { widget.changeActive(widget.selectedHint + n); },
        setFocus: function (n) { widget.changeActive(n); },
        menuSize: function () { return widget.screenAmount(); },
        length: completions.length,
        close: function () { completion.close(); },
        pick: function () { widget.pick(); }
    }));

    if (options.closeOnUnfocus !== false) {
        let closingOnBlur;
        cm.on("blur", this.onBlur = function () { closingOnBlur = setTimeout(function () { completion.close(); }, 100); });
        cm.on("focus", this.onFocus = function () { clearTimeout(closingOnBlur); });
    }

    let startScroll = cm.getScrollInfo();
    cm.on("scroll", this.onScroll = function () {
        let curScroll = cm.getScrollInfo(), editor = cm.getWrapperElement().getBoundingClientRect();
        let newTop = top + startScroll.top - curScroll.top;
        let point = newTop - (window.pageYOffset || (document.documentElement || document.body).scrollTop);
        if (!below) point += hints.offsetHeight;
        if (point <= editor.top || point >= editor.bottom) return completion.close();
        hints.style.top = newTop + "px";
        hints.style.left = (left + startScroll.left - curScroll.left) + "px";
    });

    CodeMirror.on(hints, "dblclick", function (e) {
        let t = e.target || e.srcElement;
        if (t.hintId != null) { widget.changeActive(t.hintId); widget.pick(); }
    });
    CodeMirror.on(hints, "click", function (e) {
        let t = e.target || e.srcElement;
        if (t.hintId != null) widget.changeActive(t.hintId);
    });
    CodeMirror.on(hints, "mousedown", function () {
        setTimeout(function () { cm.focus(); }, 20);
    });

    CodeMirror.signal(data, "select", completions[0], hints.firstChild);
    return true;
}

Widget.prototype = {
    close: function () {
        if (this.completion.widget != this) return;
        this.completion.widget = null;
        this.hints.parentNode.removeChild(this.hints);
        this.completion.cm.removeKeyMap(this.keyMap);

        let cm = this.completion.cm;
        if (this.completion.options.closeOnUnfocus !== false) {
            cm.off("blur", this.onBlur);
            cm.off("focus", this.onFocus);
        }
        cm.off("scroll", this.onScroll);
    },

    pick: function () {
        this.completion.pick(this.data, this.selectedHint);
    },

    changeActive: function (i) {
        i = Math.max(0, Math.min(i, this.data.list.length - 1));
        if (this.selectedHint == i) return;
        let node = this.hints.childNodes[this.selectedHint];
        node.className = node.className.replace(" CodeMirror-hint-active", "");
        node = this.hints.childNodes[this.selectedHint = i];
        node.className += " CodeMirror-hint-active";
        if (node.offsetTop < this.hints.scrollTop)
            this.hints.scrollTop = node.offsetTop - 3;
        else if (node.offsetTop + node.offsetHeight > this.hints.scrollTop + this.hints.clientHeight)
            this.hints.scrollTop = node.offsetTop + node.offsetHeight - this.hints.clientHeight + 3;
        CodeMirror.signal(this.data, "select", this.data.list[this.selectedHint], node);
    },

    screenAmount: function () {
        return Math.floor(this.hints.clientHeight / this.hints.firstChild.offsetHeight) || 1;
    }
};

export default CodeMirror.showHint