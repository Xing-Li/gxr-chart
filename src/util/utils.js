var _ = require('lodash');
const { unescapeHtml } = require("./helpers");

var taskList = [];
var taskTimeout;
export const submitTask = (task) => {
  if (!task) {
    return;
  }
  if (taskTimeout) {
    taskList.push(task);
  }
  taskTimeout = setTimeout(() => {
    task(taskCallback);
  }, 10)
}

const taskCallback = () => {
  taskTimeout = undefined;
  if (taskList.length > 0) {
    let task = taskList.splice(0, 1)[0];
    submitTask(task);
  }
}

export const ShareData = {};

export const CodeTools = [];

export const CodeOptions = {
  mode: 'javascript',
  theme: 'eclipse',
  lineNumbers: true,
  lineWrapping: true,
  viewportMargin: Infinity,
  styleSelectedText: true,
  matchBrackets: false,
  styleActiveLine: false,
  nonEmpty: false,
  foldGutter: true,
  gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
  extraKeys: {
    "F10": function (cm) {
      cm.setOption("fullScreen", !cm.getOption("fullScreen"));
    },
    "Esc": function (cm) {
      if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
    },
    'Cmd-/': 'toggleComment',
    'Ctrl-/': 'toggleComment',
  }
};

export const Themes = ['default', 'azul', 'bee-inspired', 'cool', 'macarons', 'macarons2',
  'tech-blue', 'infographic', "roma",
  "vintage", "shine", "dark"]

export const SelectProps = {
  listHeight: 32 * 6,
  getPopupContainer: () =>
    document.querySelector(".chart-setting-drawer .ant-drawer-body"),

}
export const SelectPropsDef = {
  listHeight: 32 * 6,
}
export const SortOptions = [
  {
    value: 'none',
    label: 'none',
  }, {
    value: 'xAxis',
    label: 'xAxis',
  },
  {
    value: 'yAxis_1',
    label: 'yAxis_1',
  },
];

export const sortFunc = (sortOption, source) => {
  if (!source || !source.length) {
    return;
  }
  if (sortOption && sortOption.length && !_.isEqual(sortOption, ['none'])) {
    source.sort(function (arr1, arr2) {
      if (sortOption[0] === 'xAxis') {
        return arr1[0] > arr2[0] ? 1 : -1;
      } else {
        let yIndex = parseInt(sortOption[0].replace("yAxis_", ""))
        return arr1[yIndex] > arr2[yIndex] ? 1 : -1;
      }
    })
  }
}
export const ChartTypes = {
  "line": { axis: ["xAxis", "yAxis_1"], multiY: true },
  "bar": { axis: ["xAxis", "yAxis_1"], multiY: true },
  "pie": { axis: ["value", "name"], xyAxisExclude: true },
  "scatter": { axis: ["xAxis", "yAxis_1"], multiY: true }
}
export const TypeObj = {
  "value": {
    optionLabel: "value",
    formatter: `if (value.toString().length > 4) {
    return value.toExponential(1)
      .replace("e+", "E").replace("e-", "E-");
  } else
    return value;`,
    min: `  return value.min;`,
    max: `  return value.max;`
  },
  "category": { optionLabel: "category" },
  "time": {
    optionLabel: "date",
    formatter: `var date = new Date(value);
  var texts = [(date.getMonth() + 1), date.getDate()];
  if (index === 0) {
    texts.unshift(1900 + date.getYear());
  }
  return texts.join('/');`,
    min: `  return value.min;`,
    max: `  return value.max;`
  },
  "log": { optionLabel: "other" }
}
export const Types = _.keys(TypeObj)
export const TypeMap = { "number": Types[0], "string": Types[1], "object": Types[1], "date": Types[2] }

export const getType = (propertyMap, index) => {
  let values = _.values(propertyMap);
  return values.length > index ? (TypeMap[values[index].type] || Types[1]) : Types[1];
}
export const DefaultNameLocationConfig = {
  "start": {
    nameLocation: 'start',
    grid: {
      top: '5%',
      right: '5%',
      bottom: '20%',
      left: '8%',
    }
  }, "middle": {
    nameLocation: 'middle',
    grid: {
      top: '5%',
      right: '5%',
      bottom: '20%',
      left: '8%',
    }
  }, "end": {
    nameLocation: 'end',
    grid: {
      top: '15%',
      right: '15%',
      bottom: '20%',
      left: '10%',
    }
  }
}
export const CategoryFromType = {
  main: "main",
  shareData: "shareData"
}
export const DefaultChartConfig = _.assign({
  chartKey: "default",
  categoryFrom: undefined,
  category: undefined,
  propertyMap: {},
  selected: false,
  chartTypeSelected: _.keys(ChartTypes)[0],
  source: [],
  createTime: new Date().getTime(),
  title: "",
  titleGrid: {
    left: "50%",
    top: "0%"
  },
  width: parseInt(625 * 100 / window.innerWidth),
  height: parseInt(900 * 100 / window.innerWidth),
  minMaxMap: {},
  dataZoom: true,
  xAxisName: undefined,
  xAxisType: undefined,
  yAxisName: undefined,
  yAxisType: undefined,
  xAxisNameGap: 15,
  yAxisNameGap: 15,
  xAxisFormatter: undefined,
  yAxisFormatter: undefined,
  xAxisMin: undefined,
  yAxisMin: undefined,
  xAxisMax: undefined,
  yAxisMax: undefined,
  xAxisLabelRotate: -45,
  yAxisLabelRotate: 0,
  theme: Themes[1],
  sortOption: ['yAxis_1'],
  fontScale: 20 / 30,
  chartTypes: [],
  scale: false,
}, DefaultNameLocationConfig["middle"]);

export const transforMX = (mx) => {
  mx = mx.trim();
  if (!isNaN(mx)) {
    return parseFloat(mx);
  }

  if (~['dataMin', 'dataMax'].indexOf(mx)) {
    return mx;
  }
  if (~mx.indexOf("return")) {
    return new Function(['value'], mx);
  }
  return new Function(['value'], `return eval(\`${mx}\`)`);
}

export const transforFormatter = (mx) => {
  mx = mx.trim();
  if (!isNaN(mx)) {
    return parseFloat(mx);
  }
  if (~mx.indexOf("return")) {
    return new Function(['value', 'index'], mx);
  }
  return new Function(['value', 'index'], `return eval(\`${mx}\`)`);
}

export const transfor = (chartConfig) => {
  const properties = _.keys(chartConfig.propertyMap);
  !chartConfig.minMaxMap && (chartConfig.minMaxMap = {})
  const xAxisType = chartConfig.xAxisType || getType(chartConfig.propertyMap, 0);
  const yAxisType = chartConfig.yAxisType || getType(chartConfig.propertyMap, 1);
  chartConfig.xAxisFormatter = unescapeHtml(chartConfig.xAxisFormatter || TypeObj[xAxisType].formatter);
  chartConfig.yAxisFormatter = unescapeHtml(chartConfig.yAxisFormatter || TypeObj[yAxisType].formatter);
  chartConfig.xAxisMin = unescapeHtml(chartConfig.xAxisMin || TypeObj[xAxisType].min);
  chartConfig.yAxisMin = unescapeHtml(chartConfig.yAxisMin || TypeObj[yAxisType].min);
  chartConfig.xAxisMax = unescapeHtml(chartConfig.xAxisMax || TypeObj[xAxisType].max);
  chartConfig.yAxisMax = unescapeHtml(chartConfig.yAxisMax || TypeObj[yAxisType].max);
  chartConfig.fontScale = chartConfig.fontScale || DefaultChartConfig.fontScale;
  chartConfig.chartTypes = chartConfig.chartTypes || DefaultChartConfig.chartTypes;
  chartConfig.xAxisFormatterTmp = unescapeHtml(chartConfig.xAxisFormatterTmp || TypeObj[xAxisType].formatter);
  chartConfig.xAxisMinTmp = unescapeHtml(chartConfig.xAxisMinTmp || TypeObj[xAxisType].min);
  chartConfig.xAxisMaxTmp = unescapeHtml(chartConfig.xAxisMaxTmp || TypeObj[xAxisType].max);
  chartConfig.yAxisFormatterTmp = unescapeHtml(chartConfig.yAxisFormatterTmp || TypeObj[yAxisType].formatter);
  chartConfig.yAxisMinTmp = unescapeHtml(chartConfig.yAxisMinTmp || TypeObj[yAxisType].min);
  chartConfig.yAxisMaxTmp = unescapeHtml(chartConfig.yAxisMaxTmp || TypeObj[yAxisType].max);
  chartConfig.xAxisLabelRotate = chartConfig.xAxisLabelRotate || DefaultChartConfig.xAxisLabelRotate;
  chartConfig.yAxisLabelRotate = chartConfig.yAxisLabelRotate || DefaultChartConfig.yAxisLabelRotate;
  chartConfig.xAxisName = chartConfig.xAxisName || (properties.length ? properties[0] : "");
  chartConfig.yAxisName = chartConfig.yAxisName || (properties.length > 1 ? properties.slice(1).join(",") : "");
  chartConfig.scale = chartConfig.scale || DefaultChartConfig.scale;
  return chartConfig;
}

export const sortObjectByKey = o => _.sortBy(Object.keys(o), n => n.toLowerCase()).reduce((r, k) => (r[k] = o[k], r), {});
export function showToast(message) {
  let x = document.getElementById("toast")
  let messageContainer = document.getElementById("desc")
  messageContainer.innerHTML = message
  x.className = "show";
  setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}


export function saveLink(link, title) {
  let save_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a');
  save_link.href = link;
  if (title) {
    save_link.download = title;
  }
  let event = document.createEvent('MouseEvents');
  event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
  save_link.dispatchEvent(event);
  URL.revokeObjectURL(save_link.href);
}

export async function convertToBlob(item, file) {
  let response = await fetch(item);
  let blob = await response.blob();
  return blob;
}

export function dataURItoBlob(dataURI) {
  try {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }

    //Old Code
    //write the ArrayBuffer to a blob, and you're done
    //var bb = new BlobBuilder();
    //bb.append(ab);
    //return bb.getBlob(mimeString);

    //New Code
    return new Blob([ab], { type: mimeString });
  } catch (e) {
    console.error(e)
    return null;
  }
}
const reBaseURL = /(^\w+:\/\/[^\/]+)|(^[A-Za-z0-9.-]+)\/|(^[A-Za-z0-9.-]+$)/;
export function getBaseURL(link) {
  const result = link.match(reBaseURL);
  if (!result) {
    return null;
  } else if (result[1]) {
    return `${result[1]}/`;
  } else {
    return `http://${result[2] || result[3]}/`;
  }
};