import {
  ArrowUpOutlined,
  CheckOutlined, CloseOutlined, LeftCircleOutlined, RightCircleOutlined
} from '@ant-design/icons';
import { Affix, BackTop, Button, Drawer, Dropdown, Form, Menu, Select, Switch } from 'antd';
import axios from 'axios';
import _ from 'lodash';
import React from 'react';
import MdFiles from './file/MdFiles';
import MyDropzone from './file/MyDropzone';
const { showToast, SelectPropsDef } = require("./util/utils");
const { codeTime } = require("./util/helpers");

const { Option } = Select;
const My = { label: "My Folder", value: '' };
export default class Wrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      navIndex: 1,
      mdfiles_transfer: { acceptedFiles: [], downloadNum: 0 },
      folderOpen: true,
      activeFileKeyDesc: [],
      loading: false,
      print: false,
      options: [My],
      option: My,
      toggleOn: false,
      visible: false,
      date: new Date().getTime()
    };
  }

  closeFolder = () => {
    this.setState({
      folderOpen: false,
    });
  };

  Folder = () => {
    const { navIndex, mdfiles_transfer, folderOpen, activeFileKeyDesc, loading, print, options, option,
      toggleOn, date } = this.state;
    return <Drawer title={<Select {...SelectPropsDef} style={{ width: "100%" }}
      value={option.value} onChange={(value, option) => {
        this.setState({ option: option })
      }} placeholder="Select Share Folder">
      {_.map(options, (option, index) => {
        return <Option title={option.value ? option.value : "undefined"}
          key={option.value ? option.value : "undefined"}
          value={option.value}>{option.label}</Option>
      })}
    </Select>}
      getContainer={document.getElementById("folder")}
      destroyOnClose={false}
      placement={"right"}
      onClose={this.closeFolder}
      visible={folderOpen}
      bodyStyle={{ paddingBottom: 12 }}
    >
      <MdFiles wrapper={this} userId={option.value}
        className={`${navIndex === 1 ? "" : "hide"}`} mdfiles_transfer={
          Object.assign({}, mdfiles_transfer, {
            setActiveFileKeyDesc: (activeFileKeyDesc) => {
              this.setState({ activeFileKeyDesc: activeFileKeyDesc })
            },
            getActiveFileKeyDesc: () => {
              return activeFileKeyDesc;
            },
            setPrint: (print, cb) => {
              this.setState({ print: print }, cb);
            },
            setLoading: (loading, cb) => {
              this.setState({ loading: loading }, cb);
            }
          })
        }></MdFiles>
      <BackTop target={() => { return document.getElementById("folder") }}>
        <ArrowUpOutlined />
      </BackTop>
    </Drawer>
  }

  componentDidMount() {
    axios.post(`/api/file/toUserShareJson/chart`, {})
      .then((res) => {
        if (!res.data.status) {
          showToast(res.data.message);
        }
        let arr = _.reduce(_.values(res.data.content), (arr, value, index) => {
          arr.push({ value: value._id, label: value.username })
          return arr;
        }, [My])
        this.setState({ options: arr });
      }).catch(function (error) {
        // handle error
        console.log(error);
      })
    document.addEventListener("keydown", (e) => {
      e = e || window.event;
      if (e.ctrlKey && e.key == 'p' && e.target.tagName != "TEXTAREA") {
        e.preventDefault();
        document.querySelector(".fa.fa-eye") && document.querySelector(".fa.fa-eye").click();
      }
      if (e.key == 'F2') {
        document.querySelector(".icon.fas.fa-pen") && document.querySelector(".icon.fas.fa-pen").click();
      }
      if (e.ctrlKey && e.altKey && e.key == 's') {
        document.querySelector(".icon.fas.fa-download") && document.querySelector(".icon.fas.fa-download").click();
      }
      if (e.ctrlKey && e.altKey && e.key == 'n') {
        document.querySelector(".icon.fas.fa-file") && document.querySelector(".icon.fas.fa-file").click();
      }
      if (e.ctrlKey && e.altKey && e.key == 'd') {
        document.querySelector(".icon.fas.fa-trash") && document.querySelector(".icon.fas.fa-trash").click();
      }
      if (e.ctrlKey && e.altKey && e.key == 'i') {
        document.querySelector(".import-export .import .file_input")
          && document.querySelector(".import-export .import .file_input").click();
      }
      if (e.ctrlKey && e.altKey && e.key == 'e') {
        document.querySelector(".import-export .export") && document.querySelector(".import-export .export").click();
      }
      if (e.ctrlKey && e.altKey && e.key == 'o') {
        this.openFunc()
      }
    }, false);
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    document.getElementById("drawer").innerHTML = ""
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date().getTime()
    });
  }

  openFunc = () => {
    this.setState((state) => { return { folderOpen: !state.folderOpen } }, () => {
    });
  };

  render() {
    const { navIndex, mdfiles_transfer, folderOpen, activeFileKeyDesc, loading, print, options, option,
      toggleOn, date } = this.state;
    const menu = <Menu className="import-export-menu">
      <Menu.Item>
        <div className="import">
          <i className="icon fas fa-file-import"></i>
          <input
            className="file_input" multiple
            type="file"
            title="import files(Ctrl+Alt+I)"
            accept="application/md"
            onChange={(e) => {
              this.setState({
                mdfiles_transfer: Object.assign({}, mdfiles_transfer, { acceptedFiles: e.currentTarget.files })
              });
            }}
          >
          </input>
        </div>
      </Menu.Item>
      <Menu.Item>
        <i className="icon fas fa-download" title={`Save All(Ctrl+Alt+S)`} onClick={() => {
          this.setState({
            mdfiles_transfer: Object.assign(
              {}, mdfiles_transfer, { downloadNum: mdfiles_transfer.downloadNum + 1 })
          })
        }}></i>
      </Menu.Item>
    </Menu>
    return (
      <React.Fragment>
        <MyDropzone className="dropzone-div" handleFilesFunc={(acceptedFiles) => {
          this.setState({
            mdfiles_transfer:
              Object.assign({}, mdfiles_transfer, { acceptedFiles: acceptedFiles })
          });
        }}>
          {loading && <span className="center-loading spinner-border spinner-border-sm"></span>}
          <div className={`wrapper`}>
            <div className={`d-flex justify-content-start ${print ? "hide" : ""}`}>
              <div className="col-12 d-flex justify-content-between">
                <h3 className="top-title">GXR Chart</h3>
                <Affix offsetTop={5}>
                  <div className="file-name-display text-primary" onClick={this.openFunc}>{
                    [...activeFileKeyDesc.slice(0, activeFileKeyDesc.length - 1),
                    codeTime(date, activeFileKeyDesc[activeFileKeyDesc.length - 1])
                    ].join(" ").trim()}</div>
                </Affix>
              </div>
              <div className="import-export">
                <div className="d-flex flex-rows">
                  <i className={`icon fa ${toggleOn ? "fa-chevron-circle-up" : "fa-chevron-circle-down"}`}
                    title={toggleOn ? "toggled fold codes" : "toggled open codes"} onClick={() => {
                      this.setState({ toggleOn: !this.state.toggleOn }, () => {
                        const { toggleOn } = this.state;
                        if (toggleOn) {
                          document.querySelectorAll(".fa-chevron-circle-down").forEach((currentTarget) => {
                            currentTarget.click();
                          })
                        } else {
                          document.querySelectorAll(".fa-chevron-circle-up").forEach((currentTarget) => {
                            currentTarget.click();
                          });
                        }
                      })
                    }}></i>
                  <Dropdown overlay={menu} placement="topCenter">
                    <i className={`my-tabs-btn icon far ${!folderOpen ? "fa-folder" : "fa-folder-open"}`}
                      title={`toggled ${folderOpen ? "open" : "close"} folder`}
                      onClick={this.openFunc}></i>
                  </Dropdown>
                </div>
              </div>
            </div>
            <div id="editorjs"></div>
            <this.Folder></this.Folder>
          </div>
        </MyDropzone>
      </React.Fragment >
    );
  }
}
