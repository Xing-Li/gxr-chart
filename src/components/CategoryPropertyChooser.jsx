import { MinusSquareOutlined, PlusSquareOutlined, SyncOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { Button, Select, Space, Form, Tooltip, Slider, Cascader } from 'antd';
import _ from 'lodash';
import cx from "classnames";
import PropTypes from "prop-types";
import React, { Fragment } from "react";
import { getNodesByLabel, _graph } from '../common/graphOps.js';
import { ChartTypes, SelectProps, ShareData, CategoryFromType } from "../util/utils";

const { Option } = Select;
const getCatogeries = function (selectedCategoryFrom) {
    if (!selectedCategoryFrom) return [];
    return selectedCategoryFrom === CategoryFromType.main ?
        _.uniq(_graph.nodes.map((n) => n.data.detail.type)) :
        _.reduce(ShareData, (prev, value, key) => {
            if (value instanceof Array && value.length > 0) {
                prev.push(key);
            }
            return prev;
        }, [])
}
const getProperties = function (selectedCategoryFrom, selectedCategory) {
    if (!selectedCategoryFrom) return [];
    if (selectedCategoryFrom === CategoryFromType.main) {
        let nodes = getNodesByLabel(selectedCategory);
        return _.uniq(
            _.flatten(nodes.map((n) => Object.keys(n.data.detail.data)))
        )
    } else {
        let nodes = ShareData[selectedCategory] || [];
        return _.uniq(
            _.flatten(nodes.map((o) => Object.keys(o)))
        )
    }
}

const getSelected = function (selectedCategoryFrom, selectedCategory) {
    return selectedCategoryFrom === CategoryFromType.main ?
        _.max(getNodesByLabel(selectedCategory).map((n) => n.selected)) :
        false;
}
export default class CategoryPropertyChooser extends React.Component {
    static propTypes = {
        handleCatetoryPropertySelected: PropTypes.func,
        setSelected: PropTypes.func,
        selectedCategory: PropTypes.string,
        selectedCategoryFrom: PropTypes.string,
        selectedProperties: PropTypes.array,
        chartTypeSelected: PropTypes.string,
        chartTypes: PropTypes.array,
        minMaxMap: PropTypes.object,
        propertyMap: PropTypes.object
    };

    static defaultProps = {};
    constructor(props) {
        super(props);
        const { selectedCategoryFrom, selectedCategory,
            selectedProperties, chartTypeSelected,
            chartTypes, minMaxMap, propertyMap } = this.props;
        const categories = getCatogeries(selectedCategoryFrom);
        const properties = getProperties(selectedCategoryFrom, selectedCategory);
        this.state = {
            categories: categories,
            properties: properties,
            selectedCategoryFrom: selectedCategoryFrom,
            selectedCategory: selectedCategory,
            selectedProperties: selectedProperties,
            chartTypeSelected: chartTypeSelected,
            chartTypes: chartTypes,
            minMaxMap: minMaxMap,
            add: false,
            hoverAxis: undefined,
            propertyMap: propertyMap
        }
        const { setSelected } = this.props;
        if (getSelected(selectedCategoryFrom, selectedCategory)) {
            setSelected(true);
        } else {
            setSelected(false);
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const { selectedCategoryFrom, selectedCategory, selectedProperties, chartTypeSelected, chartTypes,
            minMaxMap, propertyMap } = this.props
        const state = {}
        if (prevProps.selectedCategoryFrom !== selectedCategoryFrom) {
            _.assign(state, { selectedCategoryFrom });
        }
        if (prevProps.selectedCategory !== selectedCategory) {
            _.assign(state, { selectedCategory });
        }
        if (!_.isEqual(prevProps.selectedProperties, selectedProperties)) {
            _.assign(state, { selectedProperties });
        }
        if (!_.isEqual(prevProps.chartTypeSelected, chartTypeSelected)) {
            _.assign(state, { chartTypeSelected });
        }
        if (!_.isEqual(prevProps.chartTypes, chartTypes)) {
            _.assign(state, { chartTypes });
        }
        if (!_.isEqual(prevProps.minMaxMap, minMaxMap)) {
            _.assign(state, { minMaxMap });
        }
        if (!_.isEqual(prevProps.propertyMap, propertyMap)) {
            _.assign(state, { propertyMap });
        }
        if (!_.isEmpty(state)) {
            this.onCategoryChanged(state)
        }
    }

    onCategoryChanged = (state) => {
        this.setState(state, () => {
            const { selectedCategoryFrom, selectedCategory } = this.state;
            if (selectedCategory) {
                const properties = getProperties(selectedCategoryFrom, selectedCategory);
                this.setState({
                    properties: properties,
                }, () => {
                    const { selectedCategoryFrom, selectedCategory } = this.state;
                    const { setSelected } = this.props;
                    if (getSelected(selectedCategoryFrom, selectedCategory)) {
                        setSelected(true);
                    } else {
                        setSelected(false);
                    }
                });
            } else {
                if (!selectedCategory) {
                    this.setState({
                        properties: []
                    })
                }
            }
        })
    };


    onPropertyChanged = (index, selectedProperty, remove) => {
        const { selectedProperties } = this.state;
        const state = { selectedProperties: selectedProperties };
        if (remove) {
            selectedProperties.splice(index, 1);
        } else {
            selectedProperties[index] = selectedProperty;
            selectedProperty && _.assign(state, { hoverAxis: index })
        }
        this.updateState(state);
    };

    updateState = (state) => {
        this.setState(state, () => {
            const { handleCatetoryPropertySelected } = this.props;
            const { selectedCategoryFrom, selectedCategory, selectedProperties,
                chartTypeSelected, minMaxMap, chartTypes } = this.state;
            const { axis } = ChartTypes[chartTypeSelected];
            if (_.filter(axis, (char, index) => {
                return selectedProperties[index] === undefined;
            }).length === 0) {
                handleCatetoryPropertySelected(selectedCategoryFrom, selectedCategory, selectedProperties,
                    minMaxMap, chartTypes);
            }
        });
    }

    componentDidMount() {
    }

    render() {
        const { categories, properties, selectedCategoryFrom, selectedCategory,
            selectedProperties, chartTypeSelected, chartTypes,
            minMaxMap, add, hoverAxis, propertyMap } = this.state;
        const { axis, multiY } = ChartTypes[chartTypeSelected];
        const { setSelected, handleCatetoryPropertySelected } = this.props;
        const startIndex = selectedProperties.length > axis.length ?
            selectedProperties.length : axis.length
        const yAxis_add = "yAxis_" + startIndex;
        const options = _.filter(properties,
            (value, index) => { return !~selectedProperties.indexOf(value) });
        const propertyObj = hoverAxis !== undefined &&
            selectedProperties[hoverAxis] && propertyMap[selectedProperties[hoverAxis]];
        const minMaxObj = hoverAxis !== undefined &&
            selectedProperties[hoverAxis] && minMaxMap[selectedProperties[hoverAxis]];
        const marks = {
        }
        if (propertyObj &&
            propertyObj.min < propertyObj.max) {
            marks[`${propertyObj.min}`] = propertyObj.min;
            marks[`${propertyObj.max}`] = {
                style: {
                    color: '#f50',
                },
                label: <strong>{propertyObj.max}</strong>,
            }
        }
        const categoriesOptions = [
            {
                value: CategoryFromType.main,
                label: CategoryFromType.main,
                children: getCatogeries(CategoryFromType.main).map((value) => {
                    return {
                        value: value,
                        label: value,
                    }
                })
            },
            {
                value: CategoryFromType.shareData,
                label: CategoryFromType.shareData,
                children: getCatogeries(CategoryFromType.shareData).map((value) => {
                    return {
                        value: value,
                        label: value,
                    }
                })
            },
        ];
        return (<Space direction="vertical" size="small" >
            <Space className="d-flex flex-wrap align-content-start">
                <Form.Item label="Category" className="config-item">
                    <Cascader getPopupContainer={SelectProps.getPopupContainer}
                        options={categoriesOptions}
                        value={[selectedCategoryFrom, selectedCategory]}
                        expandTrigger="hover"
                        displayRender={(label) => {
                            return label[label.length - 1];
                        }}
                        onChange={(value) => {
                            if (value.length === 2) {
                                this.onCategoryChanged({
                                    selectedCategoryFrom: value[0],
                                    selectedCategory: value[1],
                                    selectedProperties: []
                                })
                            } else if (value.length === 0) {
                                this.onCategoryChanged({
                                    selectedCategoryFrom: undefined,
                                    selectedCategory: undefined,
                                    selectedProperties: []
                                })
                            }
                        }}

                    />
                </Form.Item>
                {
                    _.map(axis, (whichAxis, index) => {
                        return <Form.Item key={whichAxis} label={whichAxis} className={cx("config-item d-flex"
                        )}>
                            <Select {...SelectProps} allowClear={true}
                                className={cx({ "cdx-input": index === hoverAxis }, "config-input")}
                                placeholder={`Select ${whichAxis}`}
                                value={selectedProperties[index]}
                                onFocus={() => {
                                    this.setState({ hoverAxis: index });
                                }}
                                onChange={(value) => {
                                    if (value === undefined) {
                                        return;
                                    }
                                    this.onPropertyChanged(index, value)
                                }}
                                onClear={() => {
                                    this.onPropertyChanged(index, undefined)
                                }}
                            >
                                {_.map(options, (value) =>
                                    <Option title={value} key={value} value={value}>{value}</Option>)}
                            </Select>
                        </Form.Item>
                    })
                }
                <Form.Item className="config-item">
                    <SyncOutlined className="anticon-icon" title="Toggle refresh" onMouseUp={(e, setLoading) => {
                        let categoriesTmp = getCatogeries(selectedCategoryFrom);
                        let state = {
                        }
                        !_.isEqual(categoriesTmp, categories) &&
                            _.assign(state, { categories: categoriesTmp })
                        !~categoriesTmp.indexOf(selectedCategory) &&
                            _.assign(state, {
                                selectedCategory: "Select Category",
                                selectedProperties: []
                            })
                        if (!_.isEmpty(state)) {
                            this.setState(state, () => {
                            })
                        } else {
                            handleCatetoryPropertySelected(selectedCategoryFrom, selectedCategory, selectedProperties);
                        }
                        if (getSelected(selectedCategoryFrom, selectedCategory)) {
                            setSelected(true);
                        } else {
                            setSelected(false);
                        }
                    }} />
                </Form.Item>
            </Space >
            <Space className="d-flex flex-wrap align-content-start">
                {
                    _.map(_.slice(selectedProperties, axis.length), (property, index) => {
                        let addIndex = axis.length + index;
                        let key = "yAxis_" + addIndex;
                        return <Form.Item label={key} className={cx("config-item d-flex",
                        )} key={key}>
                            <div className="d-flex">
                                <Select {...SelectProps} allowClear={true}
                                    className={cx({ "cdx-input": addIndex === hoverAxis }, "config-input")}
                                    placeholder={`Select ${key}`}
                                    value={property}
                                    onFocus={() => {
                                        this.setState({ hoverAxis: addIndex });
                                    }}
                                    onChange={(value) => {
                                        if (value === undefined) {
                                            return;
                                        }
                                        this.onPropertyChanged(addIndex, value)
                                    }}
                                    onClear={() => {
                                        this.onPropertyChanged(addIndex, property, true)
                                    }}
                                >
                                    {_.map(options, (value) =>
                                        <Option title={value} key={value} value={value}>{value}</Option>)}
                                </Select>
                                <Select {...SelectProps}
                                    className="config-input" placeholder="Select chart type"
                                    value={_.isEmpty(chartTypes[addIndex]) ? chartTypeSelected : chartTypes[addIndex]} onChange={(value, option) => {
                                        chartTypes[addIndex] = value;
                                        this.updateState({ chartTypes: chartTypes })
                                    }}>
                                    {
                                        _.map(_.keys(ChartTypes), (chartType, index) => {
                                            return <Option title={chartType} key={chartType} value={chartType} >{chartType}</Option>
                                        })
                                    }
                                </Select>
                            </div>
                        </Form.Item>
                    })
                }
                {
                    add && <Form.Item label={yAxis_add}
                        className={cx("config-item d-flex",
                        )}>
                        <Select {...SelectProps}
                            key={yAxis_add}
                            className={cx({ "cdx-input": startIndex === hoverAxis }, "config-input")}
                            placeholder={`Select ${yAxis_add}`}
                            value={undefined}
                            onFocus={() => {
                                this.setState({ hoverAxis: startIndex });
                            }}
                            onChange={(value) => {
                                this.setState({ add: false }, () => {
                                    if (value === undefined) {
                                        return;
                                    }
                                    this.onPropertyChanged(startIndex, value)
                                });
                            }}
                        >
                            {_.map(options, (value) =>
                                <Option title={value} key={value} value={value}>{value}</Option>)}
                        </Select>
                        <MinusSquareOutlined className="anticon-icon" title="Toggle delete yAxis" onMouseUp={() => {
                            this.setState({ add: false });
                        }} />
                    </Form.Item>
                }
                {multiY && <PlusSquareOutlined className="anticon-icon" title="Toggle add yAxis" onMouseUp={() => {
                    this.setState({ add: true });
                }} />}
                {propertyObj &&
                    propertyObj.min < propertyObj.max &&
                    <Form.Item label={selectedProperties[hoverAxis]} className="config-item d-flex">
                        <Slider style={{ width: "200px" }} step={(propertyObj.max - propertyObj.min) / 100} range min={propertyObj.min}
                            max={propertyObj.max} value={minMaxObj ?
                                [minMaxObj.min, minMaxObj.max] :
                                [propertyObj.min, propertyObj.max]}
                            onChange={(value) => {
                                minMaxMap[selectedProperties[hoverAxis]] = {}
                                minMaxMap[selectedProperties[hoverAxis]].min = value[0];
                                minMaxMap[selectedProperties[hoverAxis]].max = value[1];
                                this.updateState({ minMaxMap: minMaxMap });
                            }}
                            marks={marks}
                        />
                    </Form.Item>
                }
            </Space>
        </Space >)
    }
}