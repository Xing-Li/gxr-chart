import React from "react";
import PropTypes from "prop-types";
export default class Spinner extends React.Component {
  static propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
    openLoading: PropTypes.bool,
    onClick: PropTypes.func,
    children: PropTypes.any,
  };

  static defaultProps = { openLoading: true };
  constructor(props) {
    super(props);
    this.state = { loading: false };
  }

  setLoading = (loading) => {
    this.setState({ loading: loading });
  };

  render() {
    const { type, className, openLoading, onClick, children, ...propsa } = this.props;
    const { loading } = this.state;
    return (
      <this.props.type
        className={`${className || ""}`}
        onClick={(e) => {
          if (openLoading && loading) return;
          openLoading && this.setLoading(true);
          onClick && onClick(e, openLoading && this.setLoading);
        }}
        {...propsa}
      >
        {openLoading && loading && (
          <span className="spinner-border spinner-border-sm"></span>
        )}
        {children}
      </this.props.type>
    );
  }
}
